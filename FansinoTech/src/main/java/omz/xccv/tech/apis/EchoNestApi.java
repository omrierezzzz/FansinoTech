package omz.xccv.tech.apis;

/**
 * Created by omrierez on 7/21/14.
 */

import android.util.Log;

import com.echonest.api.v4.Artist;
import com.echonest.api.v4.EchoNestAPI;
import com.echonest.api.v4.EchoNestException;
import com.echonest.api.v4.Params;
import com.echonest.api.v4.Song;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.utils.Utils;

public class EchoNestApi {

    private static String TAG=EchoNestAPI.class.getSimpleName();

    private static EchoNestAPI mApiInstance;
    private static EchoNestApi mThisInstance;

    private static final String APP_ID = "TCOEP9HSVJUVXM81V";
    private static final String QUERY_URL_NEWS_FEED = "http://developer.echonest.com/artist/%s/news.rss?api_key=%s";
    private static final String QUERY_URL_BLOG_FEED = "http://developer.echonest.com/artist/%s/blogs.rss?api_key=%s";
    private static final String QUERY_URL_REVIEWS_FEED = "http://developer.echonest.com/artist/%s/reviews.rss?api_key=%s";
    private static final String QUERY_URL_VIDEO_FEED = "http://developer.echonest.com/artist/%s/video.rss?api_key=%s";
    private static final String QUERY_URL_ARTIST_IMAGE = "http://developer.echonest.com/api/v4/artist/images?api_key=%s&name=%s&format=json&results=1&start=0&license=unknown";

    private XmlPullParserFactory xmlFactoryObject;
    private XmlPullParser mXmlParser;

    public static EchoNestApi getInstance()
    {
        if (mApiInstance ==null)
            mApiInstance =new EchoNestAPI(APP_ID);
        if (mThisInstance==null)
            mThisInstance=new EchoNestApi();
        return mThisInstance;
    }

    public Artist searchArtist(String artistName,String songName)
    {
        List<Artist> artists=null;
        Artist artist=null;
        Params p = new Params();
        p.set("artist",artistName);
        p.set("title",songName);
              try {
          artists = mApiInstance.searchArtists(artistName);
           List<Song> songs =mApiInstance.searchSongs(p);
           if (artists!=null&&artists.size()>0) {
               artist = artists.get(0);
               Log.i(TAG, "searchArtist "+artist.getName());
           }
           else
               Log.i(TAG, "searchArtist: no match found");

       }catch(EchoNestException e)
       {
           e.printStackTrace();

       }

        return artist;
    }




    public EchoNestApi() {
        try {
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            mXmlParser = xmlFactoryObject.newPullParser();
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            FansinoSdk.logException(e);
        }

    }

    public ArrayList<EchoNestFeedItem> xmlParse(InputStream is)
            throws IOException, XmlPullParserException {

        ArrayList<EchoNestFeedItem> results = new ArrayList<EchoNestFeedItem>();
        mXmlParser(is);
        int eventType = mXmlParser.getEventType();
        String currentTag = null;
        String link = null;
        String title = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                currentTag = mXmlParser.getName();
            } else if (eventType == XmlPullParser.TEXT) {
                if ("title".equals(currentTag)) {
                    title = mXmlParser.getText();
                }
                if ("link".equals(currentTag)) {
                    link = mXmlParser.getText();
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if ("item".equals(mXmlParser.getName())) {

                    results.add(new EchoNestFeedItem(title, link, title));
                }
            }
            eventType = mXmlParser.next();
        }
        return results;
    }

    private void mXmlParser(InputStream xml) {
        // TODO Auto-generated method stub

    }

    public InputStream HttpGet(String urlToRead) throws IOException, XmlPullParserException {
        URL url;
        HttpURLConnection conn;
        url = new URL(urlToRead);
        conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        return conn.getInputStream();


    }

    public ArrayList<EchoNestFeedItem> getFeed(String artistName, String url)
            throws JSONException, IOException, XmlPullParserException {
        ArrayList<EchoNestFeedItem> apiResponse = xmlParse(HttpGet(String.format(url,URLEncoder.encode(artistName, "utf-8"), APP_ID)));
        return apiResponse;


    }

    public void getFeeds(String artistName)
            throws JSONException, IOException, XmlPullParserException {
        getFeed(artistName, QUERY_URL_NEWS_FEED);
        getFeed(artistName, QUERY_URL_BLOG_FEED);
        getFeed(artistName, QUERY_URL_REVIEWS_FEED);
        getFeed(artistName, QUERY_URL_VIDEO_FEED);

    }

    public static void main(String args[]) {

        EchoNestApi api = new EchoNestApi();

        try {
            api.getFeeds("jay z");
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            FansinoSdk.logException(e);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            FansinoSdk.logException(e);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            FansinoSdk.logException(e);
        } catch (XmlPullParserException e) {
            // TODO Auto-generated catch block
            FansinoSdk.logException(e);
        }
    }

    class EchoNestFeedItem {

        public String title;
        public String link;
        public String source;

        public EchoNestFeedItem(String title, String link, String source) {
            this.title = title;
            this.link = link;
            this.source = source;

        }
    }

    public String getArtistImage(String artistName)
    {

       String artistImage="UNKNOWN";
        try {
            String response = Utils.inputStreamToString(HttpGet(String.format(QUERY_URL_ARTIST_IMAGE, APP_ID, URLEncoder.encode(artistName, "utf-8"))));
            JSONObject resp = new JSONObject(response);
            if (resp.getJSONObject("response").has("total"))
                if (resp.getJSONObject("response").getInt("total") > 0)
                    artistImage = resp.getJSONObject("response").getJSONArray("images").getJSONObject(0).getString("url");
        }catch(Exception e){}

       return artistImage;

    }
}

