package omz.xccv.tech.apis;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.gracenote.mmid.MobileSDK.GNConfig;
import com.gracenote.mmid.MobileSDK.GNOperations;
import com.gracenote.mmid.MobileSDK.GNSearchResponse;
import com.gracenote.mmid.MobileSDK.GNSearchResult;
import com.gracenote.mmid.MobileSDK.GNSearchResultReady;

import omz.xccv.tech.interfaces.EventConsumer;

public class GraceNoteApi {

	public static final short EVENT_ID_SONG_RECONGNIZED_FROM_FILE = 20;
	public static final short EVENT_ID_SONG_NOT_RECONGNIZED_FROM_FILE = 21;
	public static final short EVENT_ID_SONG_RECONGNIZED_FROM_MIC = 22;
	public static final short EVENT_ID_SONG_NOT_RECONGNIZED_FROM_MIC = 23;
	final String APP_ID = "13464576-62F11AABF619409FA0FB66B176543054";
	final String TAG = "Gracenote";
	final short DO_PICK_FILE = 1;
	private GNConfig config;
	private Context mContext;
	private EventConsumer myOutEventConsumer;

	public GraceNoteApi(Context con, EventConsumer eventConsumer) {
		config = GNConfig.init(APP_ID, con);
		mContext = con;
		myOutEventConsumer = eventConsumer;
	}

	public void recognizeAudioFromTrackId(long trackid) {
		
		RecognizeFromFile task = new RecognizeFromFile(getRealPathFromId(trackid));
		task.doFingerprint();
	}

    public void recognizeAudioFromMic() {

        RecognizeFromMic task = new RecognizeFromMic();
        task.doFingerprint();
    }

	public String getRealPathFromURI(Uri contentUri) {
		String[] proj = { MediaStore.Images.Media.DATA };
		CursorLoader loader = new CursorLoader(mContext, contentUri, proj,
				null, null, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	
	public String getRealPathFromId(long id) {
		String[] proj = { MediaStore.Audio.Media.DATA };
		Uri uri = Uri.parse("content://media/external/audio/media/");
		CursorLoader loader = new CursorLoader(mContext, uri, proj,
				MediaStore.Audio.Media._ID + "=?", new String[]{""+id}, null);
		Cursor cursor = loader.loadInBackground();
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
	
	class RecognizeFromMic implements GNSearchResultReady {
		void doFingerprint() {
			GNOperations.recognizeMIDStreamFromMic(this, config);
		}

		public void GNResultReady(GNSearchResult result) {
			if (result.isFingerprintSearchNoMatchStatus()) {

				myOutEventConsumer.event(
						EVENT_ID_SONG_NOT_RECONGNIZED_FROM_MIC, null);
			} else {

				GNSearchResponse response = result.getBestResponse();
				myOutEventConsumer.event(EVENT_ID_SONG_RECONGNIZED_FROM_MIC,
						response);
			}
		}
	}

	class RecognizeFromFile implements GNSearchResultReady {

		private String mPathToFile;

		public RecognizeFromFile(String pathToFile) {
			mPathToFile = pathToFile;
		}

		void doFingerprint() {
			GNOperations.recognizeMIDFileFromFile(this, config, mPathToFile);
		}

		public void GNResultReady(GNSearchResult result) {
			if (result.isFingerprintSearchNoMatchStatus()) {
				myOutEventConsumer.event(
						EVENT_ID_SONG_NOT_RECONGNIZED_FROM_FILE, null);
			} else {
				GNSearchResponse response = result.getBestResponse();
                myOutEventConsumer.event(EVENT_ID_SONG_RECONGNIZED_FROM_FILE,
						response);


			}
		}
	}

}
