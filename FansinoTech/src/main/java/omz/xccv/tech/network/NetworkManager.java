package omz.xccv.tech.network;

import android.content.Context;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import omz.xccv.tech.BuildConfig;
import omz.xccv.tech.utils.LogUtils;

/**
 * Created by omrierez on 6/28/15.
 */
public class NetworkManager {
    private static final String TAG = NetworkManager.class.getSimpleName();
    private static final int HTTP_DISK_CACHE_MAX_SIZE = 50 * 1024 * 1024; // 50MB
    public static final String AUTHORIZATION_HEADER_CONTENT = "BASIC b216LnhjY3ZsYWIudGVjaC5wYWxvYWx0by5yb2Nrcw==";
    public static final String API_KEY = "TO BE FILLED LATER";
    private Context appContext;

    private static OkHttpClient mHttpClient;

    public NetworkManager(Context appCon) {
        this.appContext = appCon;
    }

    private OkHttpClient getHttpClient()

    {
        if (mHttpClient == null) {
            mHttpClient = new OkHttpClient();
            File cacheDir = new File(appContext.getCacheDir(), "httpCache");
            Cache cache = new Cache(cacheDir, HTTP_DISK_CACHE_MAX_SIZE);
            mHttpClient.setCache(cache);
        }
        return mHttpClient;
    }


    public String doPostSync(String url, RequestBody body, HashMap<String, String> headers) {
        LogUtils.logDebug(TAG, String.format("DO POST SYNC url:%s\nparams:%s", url, body));
        Request.Builder builder = new Request.Builder();
        builder = setHeaders(builder, headers);
        Request request = builder
                .url(url).post(body)
                .build();
        Response response;
        String responseString;
        try {
             response = getHttpClient().newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            responseString=response.body().string();
        } catch (IOException e) {
            LogUtils.logError(TAG, "doPost error", e);
            return null;
        }
        LogUtils.logDebug(TAG, String.format("DO POST SYNC RESPONSE\n",responseString));
        return responseString;
    }




    public void doPostASync(String url, RequestBody body, HashMap<String, String> headers, Callback callback) {
        LogUtils.logDebug(TAG, String.format("DO POST ASYNC url:%s\nparams:%s", url, body));
        Request.Builder builder = new Request.Builder();
        builder = setHeaders(builder, headers);
        Request request = builder.url(url).post(body)
                .build();
        getHttpClient().newCall(request).enqueue(callback);

    }

    public String doGetSync(String url, HashMap<String, String> params, HashMap<String, String> headers) throws UnsupportedEncodingException {

        LogUtils.logDebug(TAG, String.format("DO GET SYNC url:%s\nparams:%s", url, params));
        url = addParamsToGetUrl(url, params);
        Request.Builder builder = new Request.Builder();
        builder = setHeaders(builder, headers);
        Request request = builder
                .url(url)
                .build();
        Response response;
        String responseString;
        try {
             response = getHttpClient().newCall(request).execute();
            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
            responseString =response.body().string();
        } catch (IOException e) {
            LogUtils.logError(TAG, "doPost error", e);
            return null;
        }
        LogUtils.logDebug(TAG, String.format("DO GET SYNC RESPONSE\n", responseString));
        return responseString;
    }

    public void doGetASync(String url, HashMap<String, String> params, HashMap<String, String> headers, Callback callback) throws UnsupportedEncodingException {
        LogUtils.logDebug(TAG, String.format("DO GET ASYNC url:%s\nparams:%s", url, params));
        url = addParamsToGetUrl(url, params);
        Request.Builder builder = new Request.Builder();
        builder = setHeaders(builder, headers);
        Request request = builder.url(url)
                .build();
        getHttpClient().newCall(request).enqueue(callback);
    }

    private String addParamsToGetUrl(String url, HashMap<String, String> params) throws UnsupportedEncodingException {
        url += "?";
        for (String key : params.keySet())
            url += key + "=" + URLEncoder.encode(params.get(key), "UTF-8") + "&";
        url = url.substring(0, url.length() - 1);
        return url;
    }

    private Request.Builder setHeaders(Request.Builder builder, HashMap<String, String> headers) {

        builder.header("Authorization", AUTHORIZATION_HEADER_CONTENT);
        builder.header("API-KEY", API_KEY);
        builder.header("App-Version","Android "+ BuildConfig.VERSION_NAME);

        if (headers != null) {
            for (String key : headers.keySet())
                builder.header(key, headers.get(key));
        }
        return builder;
    }


}
