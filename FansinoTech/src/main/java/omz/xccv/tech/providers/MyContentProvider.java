package omz.xccv.tech.providers;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.HashMap;

import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.R;
import omz.xccv.tech.utils.LogUtils;

public class MyContentProvider extends ContentProvider {

	private static DatabaseHelper dbHelper;
	// private SQLiteDatabase //sqlDB;
	private static final String DATABASE_NAME = "FansinoMusicMetadata.db";
	private static final int DATABASE_VERSION = 1;
	public static final String TABLE_NAME_MUSIC_METADATA = "MUSIC_METADATA";


    public  static HashMap<String,Integer> getMostHeardArtistsFromDb()
    {

        HashMap<String,Integer> mostHeardArtistsAndNums=new HashMap<String, Integer>();
        Cursor cur=dbHelper.getReadableDatabase().query(TABLE_NAME_MUSIC_METADATA, new String[]{MyLibTablesDef.MusicMetaDataColumns.ARTIST_NAME,String.format("COUNT(%s)",MyLibTablesDef.MusicMetaDataColumns.ARTIST_NAME)},null,null,MyLibTablesDef.MusicMetaDataColumns.ARTIST_NAME,null,null);
        String artistName=null;
        int num;

        if( cur.moveToFirst())
        {
           do{ artistName=cur.getString(cur.getColumnIndex(MyLibTablesDef.MusicMetaDataColumns.ARTIST_NAME));
            num=cur.getInt(1);
            if(!mostHeardArtistsAndNums.containsKey(artistName))
                mostHeardArtistsAndNums.put(artistName,num);
            else {
                num=mostHeardArtistsAndNums.get(artistName);
                num++;
                mostHeardArtistsAndNums.put(artistName,num);

            }}
           while(cur.moveToNext());


        }
        cur.close();
        return mostHeardArtistsAndNums;


    }


    // LIST OF POSSIBLE URIS
	public static final short MH_CONTENT_URI = 0;
	public static final short MH_INSERT = 1;
	public static final short MH_UPDATE_ONE = 2;
	public static final short MH_UPDATE_SPECIFIC = 3;
	public static final short MH_RAW_ARTISTS_PLAYTIME_URL = 3;

	private static  UriMatcher uriMatcher;


    public void initUriMatcher(Context con)
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        String authority=con.getResources().getString(R.string.lib_provider_authority);

        uriMatcher.addURI(authority, "music_history/insert",
                MH_INSERT);
        uriMatcher
                .addURI(authority,
                        "music_history/rawArtistsPlayTime",
                        MH_RAW_ARTISTS_PLAYTIME_URL);

        uriMatcher.addURI(authority, "music_history/",
                MH_CONTENT_URI);

        uriMatcher.addURI(authority, "music_history/#", MH_UPDATE_ONE);


    }
	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		private String createTableStatment(String tableName,
				String[] tableColumns, String[] tableColumnsTypes) {
			String createStatement = "Create table " + tableName + "( ";
			for (int i = 0; i < tableColumns.length; i++)
				createStatement += tableColumns[i] + " " + tableColumnsTypes[i]
						+ ",";
			createStatement = createStatement.substring(0,
					createStatement.length() - 1);
			createStatement += ");";
			return createStatement;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// create table that stores which music the user listened to
			db.execSQL(createTableStatment(TABLE_NAME_MUSIC_METADATA,
					MyLibTablesDef.MusicMetaDataColumns.TABLES_DEF_COLUMNS,
					MyLibTablesDef.MusicMetaDataColumns.TABLES_DEF_COLUMNS_TYPES));

			LogUtils.logInfo("MyContentProvider", "DatabaseHelper onCreate");
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			LogUtils.logInfo("MyContentProvider", "DatabaseHelper onUpgrade");

			db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_MUSIC_METADATA);


			onCreate(db);

		}
	}

	@Override
	public boolean onCreate() {
        initUriMatcher(getContext());
		dbHelper = new DatabaseHelper(getContext());

		return (dbHelper == null) ? false : true;
	}

	@Override
	public int delete(Uri uri, String s, String[] as) {

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		switch (uriMatcher.match(uri)) {

		default:
			throw new SQLException("Failed to delete row into " + uri);
		}


	}

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues contentvalues) {
		// get database to insert records
		// sqlDB = dbHelper.getWritableDatabase();
		// insert record in user table and get the row number of recently
		// inserted record

		long newRowID = 0;
		switch (uriMatcher.match(uri)) {



		case MH_INSERT:
			newRowID = dbHelper.getWritableDatabase().insert(
					TABLE_NAME_MUSIC_METADATA, "", contentvalues);
			if (newRowID > 0) {
				Uri rowUri = ContentUris
						.appendId(
								MyLibTablesDef.MusicMetaDataColumns.CONTENT_URI
										.buildUpon(),
								newRowID).build();
				 getContext().getContentResolver().notifyChange(rowUri, null);
				// dbHelper.close();
				return rowUri;
			}
			break;

		default:
			throw new SQLException("Failed to insert row into " + uri);

		}

		
		return ContentUris.appendId(
				MyLibTablesDef.MusicMetaDataColumns.CONTENT_URI.buildUpon(), 0)
				.build();
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = null;
		switch (uriMatcher.match(uri)) {
		case MH_CONTENT_URI:
			qb.setTables(TABLE_NAME_MUSIC_METADATA);
			c = queryInternal(uri, projection, selection, selectionArgs,
					sortOrder, qb, db);
			break;

		case MH_RAW_ARTISTS_PLAYTIME_URL:
			qb.setTables(TABLE_NAME_MUSIC_METADATA);
			c = db.rawQuery(
					"select artistName  as 'artist' ,SUM (playTimeInMIlli)  as 'totalplaytime' FROM "
							+ TABLE_NAME_MUSIC_METADATA + " GROUP BY 1", null);
			break;

		default:
			throw new SQLException("Failed to get row for uri " + uri);

		}
		try {

			// c.setNotificationUri(getContext().getContentResolver(), uri);
			return c;
		} catch (Exception e) {
			FansinoSdk.logException(e);
		}
		return null;

	}

	private Cursor queryInternal(Uri uri, String[] projection,
			String selection, String[] selectionArgs, String sortOrder,
			SQLiteQueryBuilder qb, SQLiteDatabase db) {
		Cursor c = qb.query(db, projection, selection, selectionArgs, null,
				null, sortOrder);
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c;
	}

	@Override
	public int update(Uri uri, ContentValues contentvalues, String where,
			String[] whereArgs) {

		int numOfRowsUpdated = 0;
		switch (uriMatcher.match(uri)) {
		case MH_UPDATE_ONE:
			numOfRowsUpdated = dbHelper.getWritableDatabase().update(
					TABLE_NAME_MUSIC_METADATA, contentvalues, "_id=?", new String[]{uri.getLastPathSegment()});

			break;

		case UriMatcher.NO_MATCH:
            LogUtils.logError("FansinoContentProvider", "No Match for:" + uri);
			break;
		default:
			throw new SQLException("Failed to get row for uri " + uri);
			

		}

		getContext().getContentResolver().notifyChange(uri, null);

		return numOfRowsUpdated;
	}
}