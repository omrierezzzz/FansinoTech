package omz.xccv.tech.providers;


import android.content.Context;
import android.net.Uri;
import android.provider.BaseColumns;

import omz.xccv.tech.R;


public class MyLibTablesDef {

    public static MusicMetaDataColumns musicMetaDataColumns;
    public static String AUTHORITY=null;


    public static void init(Context con)
    {
        AUTHORITY=con.getResources().getString(R.string.lib_provider_authority);
        musicMetaDataColumns=new MusicMetaDataColumns(AUTHORITY);

    }



    // BaseColumn contains _id.
	public static  class MusicMetaDataColumns implements BaseColumns {

        public static  Uri CONTENT_URI;
        public static  Uri RAW_QUERY_ARTISTS_PLAYTIME_URI;
        public static  Uri INSERT_URI;
        public static  Uri UPDATE_URI;
	// Table column
	public static final String ID = "_id";
	public static final String TRACK_NAME = "trackName";
	public static final String ARTIST_NAME = "artistName";
	public static final String RELEASE = "release";
	public static final String SERVICE_NAME = "serviceName";
	public static final String TIME_AND_DATE_LISTENED_IN_MILLI = "timeAndDateListenedInMilli";
	public static final String ID_IN_SERVER = "idInServer";
	public static final String PLAY_TIME = "playTimeInMIlli";
	public static final String SENT_TO_SERVER_STATE = "sentToServerState";
	public static final String IS_PLAYING_NOW = "isPlayingNow";

		// Table column

	public static final String[] TABLES_DEF_COLUMNS = { ID, TRACK_NAME,
		ARTIST_NAME,RELEASE,SERVICE_NAME, TIME_AND_DATE_LISTENED_IN_MILLI,ID_IN_SERVER,PLAY_TIME,SENT_TO_SERVER_STATE,IS_PLAYING_NOW };
    public static final String[] TABLES_DEF_COLUMNS_TYPES = {
		"INTEGER PRIMARY KEY AUTOINCREMENT", "TEXT", "TEXT", "TEXT", "TEXT",
		"LONG","INTEGER DEFAULT -1","LONG DEFAULT 0","INTEGER DEFAULT 0","INTEGER DEFAULT 1" };



        public MusicMetaDataColumns(String authority)
        {
            CONTENT_URI = Uri.parse("content://"
                    + authority + "/music_history");
            RAW_QUERY_ARTISTS_PLAYTIME_URI = Uri.parse("content://"
                    + authority + "/music_history/rawArtistsPlayTime");
            UPDATE_URI = Uri.parse("content://" + authority
                    + "/music_history/update");
            INSERT_URI = Uri.parse("content://" + authority
                    + "/music_history/insert");
        }

    }

	}