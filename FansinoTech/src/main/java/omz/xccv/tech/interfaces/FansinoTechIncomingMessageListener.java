package omz.xccv.tech.interfaces;

import org.json.JSONObject;

public interface FansinoTechIncomingMessageListener {

	public void incomingMessage(JSONObject jsonObject);
}
