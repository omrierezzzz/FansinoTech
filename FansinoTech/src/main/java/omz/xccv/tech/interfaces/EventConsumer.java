package omz.xccv.tech.interfaces;

public interface EventConsumer {

	//last id used :35
	public void event(int eventId);
	public void event(int eventId, Object data);
	public void event(int eventId, Object[] data);
}
