package omz.xccv.tech.interfaces;

import android.net.Uri;

import omz.xccv.tech.models.Metadata;

public interface MetadataLocalDBCallbacks {


	void metadataSentToServer(Metadata metadata);
	void metadataNotSentToServer(Metadata metadata, Exception e);
	void metadataUpdatedInServer(Metadata metadata);
	void metadataNotUpdatedInServer(Metadata metadata, Exception e);
	void insertMetadataComplete(Uri uri);
	void updateMetadataNotPlayingNowComplete(Uri uri);
	void updateMetadataPlayingTime(Uri uri);


		
}
