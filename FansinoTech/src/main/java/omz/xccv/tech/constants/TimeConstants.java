package omz.xccv.tech.constants;

/**
 * Created by Ignasi on 18/09/14.
 */
public class TimeConstants {
    public static final long SECOND = 1000;
    public static final long MINUTE = 60 * SECOND;
    public static final long HOUR = 60 * MINUTE;
    public static final long DAY = 24 * HOUR;
    public static final long YEAR = 365 * DAY;
}
