package omz.xccv.tech.models;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.Serializable;

import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.apis.EchoNestApi;
import omz.xccv.tech.interfaces.MetadataLocalDBCallbacks;
import omz.xccv.tech.network.NetworkManager;
import omz.xccv.tech.providers.MyLibTablesDef;
import omz.xccv.tech.utils.FansinoTechApi;
import omz.xccv.tech.utils.LogUtils;

public class Metadata implements Serializable, Cloneable {

    public static final String TAG = "Metadata";
    private final static String REST_PATH_METADATA = "/metadata";
    private final static String REST_PATH_UPDATE_FAN_STOP_LISTENING = REST_PATH_METADATA + "/fanStopListening";
    private final static String REST_PATH_INSERT_METADATA = REST_PATH_METADATA + "/insert";
    private final static String REST_PATH_UPDATE_METADATA_PLAYTIME = REST_PATH_METADATA + "/update/playingTime";
    private static NetworkManager mNetworkManager;
    public static final short INSERT_MUSIC_METADATA = 10;
    public static final short UPDATE_MUSIC_METADATA_NOT_PLAYING_NOW = 11;
    public static final short UPDATE_MUSIC_METADATA_PLAYTIME = 12;

    public static int METADATA_STATE_NOT_SENT = 0;
    public static int METADATA_STATE_SENT = 1;
    public static int METADATA_STATE_NOT_UPDATED = 2;
    public static int METADATA_STATE_UPDATED = 3;

    private static final long serialVersionUID = 3467586959563408841L;
    public int id_in_server = -1;
    public long id_on_media_lib = -1;
    public String artistName;
    public String albumName;
    public String trackName;
    public String serviceName;
    public int serviceId;

    public int sentToServerState = METADATA_STATE_NOT_SENT;
    public long playTimeInSeconds = -1;
    public long whenPlayedInMilli = -1;
    public Uri localUri;

    private MetadataLocalDBCallbacks mMetadataInterface;
    private static Context appContext;


    public String toString() {
        return artistName + "\t" + trackName + "\t" + albumName + "\t" + serviceName;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        return (Metadata) super.clone();
    }


    public Metadata(String artistName, String albumName, String trackName, String serviceName,
                    long id_on_media_lib, long whenPlayedInMilli, int serviceId, MetadataLocalDBCallbacks myInterface, Context appContext) {

        if (mNetworkManager == null)
            mNetworkManager = new NetworkManager(appContext);
        EchoNestApi.getInstance().searchArtist(artistName, trackName);


        this.artistName = artistName;


        if (albumName != null)
            this.albumName = albumName;
        else
            this.albumName = "UNKNOWN";
        this.trackName = trackName;
        this.serviceName = serviceName;
        this.id_on_media_lib = id_on_media_lib;
        this.whenPlayedInMilli = whenPlayedInMilli;
        this.serviceId = serviceId;
        mMetadataInterface = myInterface;


    }


    public boolean compareTo(Metadata metadata) {

        LogUtils.logInfo(TAG, toString());

        if (metadata == null)
            return false;
        else if (this.id_on_media_lib != metadata.id_on_media_lib)
            return false;
        else if (this.albumName.compareTo(metadata.albumName) != 0)
            return false;
        else if (this.trackName.compareTo(metadata.trackName) != 0)
            return false;
        else if (this.artistName.compareTo(metadata.artistName) != 0)
            return false;
        return true;

    }

    public void insertMusicMeatadata(ContentResolver cp) {
        try {
            ContentValues values = new ContentValues();
            values.put(MyLibTablesDef.MusicMetaDataColumns.ARTIST_NAME, artistName);
            values.put(MyLibTablesDef.MusicMetaDataColumns.RELEASE, albumName);
            values.put(MyLibTablesDef.MusicMetaDataColumns.TRACK_NAME, trackName);
            values.put(MyLibTablesDef.MusicMetaDataColumns.SERVICE_NAME, serviceName);
            values.put(MyLibTablesDef.MusicMetaDataColumns.TIME_AND_DATE_LISTENED_IN_MILLI, whenPlayedInMilli);


            Uri uri = cp.insert(MyLibTablesDef.MusicMetaDataColumns.INSERT_URI, values);
            localUri = uri;
            mMetadataInterface.insertMetadataComplete(uri);
        } catch (Exception e) {
            FansinoSdk.logException(e);
        }

    }

    public void updateMusicMeatadataNotPlayingNow(ContentResolver cp) {
        try {
            ContentValues values = new ContentValues();
            values.put(MyLibTablesDef.MusicMetaDataColumns.IS_PLAYING_NOW, 0);
            int numOfRowsUpdated = cp.update(localUri, values, null, null);
            mMetadataInterface.updateMetadataNotPlayingNowComplete(localUri);

        } catch (Exception e) {
            FansinoSdk.logException(e);
        }

    }

    public void updateIdInServerInLOcalDb(ContentResolver cp) {
        try {
            ContentValues values = new ContentValues();
            values.put(MyLibTablesDef.MusicMetaDataColumns.ID_IN_SERVER, id_in_server);
            int numOfRowsUpdated = cp.update(localUri, values, null, null);
            mMetadataInterface.updateMetadataNotPlayingNowComplete(localUri);

        } catch (Exception e) {
            FansinoSdk.logException(e);
        }

    }

    public void updateMusicMeatadataPLayingTime(ContentResolver cp) {
        try {
            ContentValues values = new ContentValues();
            LogUtils.logInfo("updateMusicMeatadataPLayingTime", "playingTime:" + playTimeInSeconds);
            values.put(MyLibTablesDef.MusicMetaDataColumns.PLAY_TIME, playTimeInSeconds);
            int numOfRowsUpdated = cp.update(localUri, values, null, null);
            mMetadataInterface.updateMetadataPlayingTime(localUri);

        } catch (Exception e) {
            FansinoSdk.logException(e);
        }

    }

    public void metadataUpdateState(int state, ContentResolver cp) {
        ContentValues values = new ContentValues();
        values.put(MyLibTablesDef.MusicMetaDataColumns.SENT_TO_SERVER_STATE, state);
        int numOfRowsUpdated = cp.update(localUri, values, null, null);

    }

    public void sendMetadata(
            final int fanID,
            final MetadataLocalDBCallbacks metaDataServerCallbacksInterface, ContentResolver cr) {
        try {
            RequestBody params = new FormEncodingBuilder()
                    .add("artist", artistName)
                    .add("fanID", "" + fanID)
                    .add("trackName", trackName)
                    .add("albumName", albumName)
                    .add("serviceName", "" + serviceId)
                    .build();
            String response = mNetworkManager.doPostSync(FansinoTechApi.SERVER_ADRRESS + REST_PATH_INSERT_METADATA, params, null);
            JSONObject respo = new JSONObject(response);
            id_in_server = respo.getInt("id");
            updateIdInServerInLOcalDb(cr);
            metaDataServerCallbacksInterface
                    .metadataSentToServer(this);
            // TODO Auto-generated catch block

        } catch (Exception e) {
            metaDataServerCallbacksInterface
                    .metadataNotSentToServer(this, e);
        }
    }

    public void updatePlayTimeMetadata(final int fanID,
                                       final MetadataLocalDBCallbacks metaDataServerCallbacksInterface) {
        try {
            RequestBody params = new FormEncodingBuilder()
                    .add("artist", artistName)
                    .add("fanID", "" + fanID)
                    .add("metadataID", "" + id_in_server)
                    .add("playTime", "" + playTimeInSeconds)
                    .add("serviceName", "" + serviceId)
                    .build();
            mNetworkManager.doPostSync(FansinoTechApi.SERVER_ADRRESS + REST_PATH_UPDATE_METADATA_PLAYTIME, params, null);
            metaDataServerCallbacksInterface
                    .metadataUpdatedInServer(this);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // FansinoSdk.logException(e);
            metaDataServerCallbacksInterface
                    .metadataNotUpdatedInServer(this, e);
        }

    }
    public void updateFanStopedListening(final int fanID) {
        try {
            RequestBody params = new FormEncodingBuilder()
                    .add("artist", artistName)
                    .add("fanID", "" + fanID)
                    .build();
            mNetworkManager.doPostSync(FansinoTechApi.SERVER_ADRRESS + REST_PATH_UPDATE_FAN_STOP_LISTENING, params, null);

        } catch (Exception e) {
            FansinoSdk.logException(e);
        } finally {

        }
    }


}
