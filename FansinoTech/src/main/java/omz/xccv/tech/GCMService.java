package omz.xccv.tech;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.Semaphore;

import omz.xccv.tech.interfaces.FansinoTechIncomingMessageListener;
import omz.xccv.tech.interfaces.ServerCallbacks;
import omz.xccv.tech.utils.LogUtils;
import omz.xccv.tech.utils.FansinoTechApi;
import omz.xccv.tech.utils.SerializationObjectsUtil;
import omz.xccv.tech.utils.UserData;
import omz.xccv.tech.utils.Utils;

public class GCMService extends GCMBaseIntentService implements ServerCallbacks {

    public final static String GCM_SENDER_ID = "400389576266,992648512659";
    public final static String TAG = "FansinoTechGCMService";
    public static final String INPUT_PUSH_MESSAGE = "data";

    public static Semaphore sFacebookSemaphore = new Semaphore(0);

    public static boolean isDummyArtist = false;

    public static FansinoTechIncomingMessageListener appMessagesListener = null;


    public GCMService() {
        super(GCM_SENDER_ID);
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

    }

    @Override
    public void onStart(Intent intent, int startId) {
        // TODO Auto-generated method stub
        super.onStart(intent, startId);

    }


    @Override
    protected void onMessage(Context context, Intent intent) {
        // TODO Auto-generated method stub

        try {
            Bundle bundle = intent.getExtras();
            String data = bundle.getString("data");
            JSONObject jobj = new JSONObject(data);
            LogUtils.logInfo(TAG + ".onMessage", jobj.toString());

            bundle.putString(INPUT_PUSH_MESSAGE, jobj.toString());

            if (appMessagesListener != null)
                appMessagesListener.incomingMessage(jobj);
            else

            {
                Intent broadcastIntent = new Intent(context.getResources().getString(R.string.action_incoming_push_message));
                broadcastIntent.putExtras(bundle);
                LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
                manager.sendBroadcast(broadcastIntent);

            }


        } catch (Exception e) {
            FansinoSdk.logException(e);
        }

    }


    @Override
    protected void onError(Context context, String errorId) {
        // TODO Auto-generated method stub
        LogUtils.logInfo("GCM onError", errorId);
    }


	@Override
	protected void onRegistered(Context context, String registrationId) {
		// TODO ADD HANDELING IF THE USER ISNT REGISTERING WITH SOCIAL LOGIN

		try {
			LogUtils.logInfo("GCM on" + "registered", registrationId);
            registerUserInServer(context, registrationId,this);


		} catch (Exception e) {
			FansinoSdk.logException(e);

		}
	}

    public static void registerUserInServer(final Context appCon, final String registrationId,final ServerCallbacks callbacks) {

        new AsyncTask<String, Void, String>() {


            @Override
            protected String doInBackground(String... params) {
                SharedPreferences mPreferences = PreferenceManager
                        .getDefaultSharedPreferences(appCon);
                UserData userData=(UserData) SerializationObjectsUtil.stringToObject(mPreferences.getString(UserData.PREFS_KEY_USER_PROFILE, ""));
                FansinoTechApi sts=new FansinoTechApi(appCon);
                sts.registerUser(mPreferences.getString(UserData.PREFS_KEY_ROLE, ""), userData.displayName, userData.country, userData.gender, 0, 0, 0, registrationId, userData.validatedId, userData.imageUrl, userData.email, userData.birthday, userData.city, callbacks, appCon);
                Utils.getGCMPreferences(appCon).edit().putString(Utils.PROPERTY_REG_ID,registrationId).commit();
                return null;

            }


        }.execute();




    }


    @Override
	protected void onUnregistered(Context context, String registrationId) {
		// TODO Auto-generated method stub
		LogUtils.logInfo("GCM onUnregistered", registrationId);

	}

	@Override
	public void userRegistrationFinishedOK(int userID) {
		// TODO Auto-generated method stub
		Utils.setIntInPrefs(getApplicationContext(),Utils.PREFS_KEY_USER_ID, userID);
        sFacebookSemaphore.release();
	}

	@Override
	public void userRegistrationFinishedERROR(String error) {
		// TODO scheduale user registration to server later
        LogUtils.logError("GCMService", "userRegistrationFinishedERROR:" + error);
	}


    public static void registerInBackground(final Context context) {
        new AsyncTask<Void,Void,String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                String regid;
                GoogleCloudMessaging gcm=null;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                   // sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context,regid);
                } catch (IOException ex) {
                    ex.printStackTrace();

                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);

    }

    public static void storeRegistrationId(Context con,String regId) {
        final SharedPreferences prefs = Utils.getGCMPreferences(con);
        int appVersion = Utils.getAppVersion(con);
        LogUtils.logInfo(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Utils.PROPERTY_REG_ID, regId);
        editor.putInt(Utils.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }





}
