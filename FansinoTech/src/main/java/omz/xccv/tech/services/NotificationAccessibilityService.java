package omz.xccv.tech.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.ActivityManager;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.widget.RemoteViews;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.music.service.parsers.Bandcamp;
import omz.xccv.tech.music.service.parsers.BeatsMusic;
import omz.xccv.tech.music.service.parsers.EightTracks;
import omz.xccv.tech.music.service.parsers.IHeartRadio;
import omz.xccv.tech.music.service.parsers.JangoRadio;
import omz.xccv.tech.music.service.parsers.MixCloud;
import omz.xccv.tech.music.service.parsers.Pandora;
import omz.xccv.tech.music.service.parsers.Rdio;
import omz.xccv.tech.music.service.parsers.Seriusxm;
import omz.xccv.tech.music.service.parsers.SlackerRadio;
import omz.xccv.tech.music.service.parsers.Songza;
import omz.xccv.tech.music.service.parsers.SonyMusic;
import omz.xccv.tech.music.service.parsers.Spotify;
import omz.xccv.tech.music.service.parsers.Tunein;
import omz.xccv.tech.music.service.parsers.YouTube;
import omz.xccv.tech.providers.MyLibTablesDef;
import omz.xccv.tech.receivers.MyMediaMReceiver;
import omz.xccv.tech.utils.LogUtils;

public class NotificationAccessibilityService extends AccessibilityService {


    public static final String IS_FROM_ACCESSIBILITY = "isFromAccessibility";
    public static String currentPackageNamePlaying = "UNKNOWN SERVICE";

    public static final String EXTRA_TEXT = "android.text";
    public static final String EXTRA_SUB_TEXT = "android.subText";
    public static final String EXTRA_LARGE_ICON = "android.largeIcon";
    public static final String EXTRA_INFO_TEXT = "android.infoText";
    public static final String EXTRA_TITLE = "android.title";

    public static final ArrayList<String> MUSIC_SERVICES_WHICH_NEED_ACCESSIBILITY=new ArrayList<String>(
            Arrays.asList(Pandora.PKG_NAME, Spotify.PKG_NAME,
            BeatsMusic.PKG_NAME, Songza.PKG_NAME, Seriusxm.PKG_NAME,
            Tunein.PKG_NAME, SonyMusic.PKG_NAME, Bandcamp.PKG_NAME, JangoRadio.PKG_NAME, Rdio.PKG_NAME,
            IHeartRadio.PKG_NAME,MixCloud.PKG_NAME,SlackerRadio.PKG_NAME,EightTracks.PKG_NAME));


    private ArrayList<String> blackListOfLyricsServices = new ArrayList<String>(
            Arrays.asList("com.musixmatch.android.lyrify"));

    private final static String TAG = "NotificationAccessibilityService";

    public static String getCurrentPackageNamePlaying()

    {
        if (currentPackageNamePlaying.compareTo("UNKNOWN SERVICE")!=0)
          return currentPackageNamePlaying;
        else
            return null;

    }


    private void performAppRecognition(String action) {

        /**
         * Attempt 6: PackageManager + queryBroadcastReceiver 2
         */
        final PackageManager pm = getPackageManager();
        Intent intentToCheck = new Intent(Intent.ACTION_MEDIA_BUTTON);
        List<ResolveInfo> listCheckedIntent = pm.queryBroadcastReceivers(
                intentToCheck, 0);
        List<String> listMediaPlayer = new ArrayList<String>();
        for (ResolveInfo info : listCheckedIntent) {
            listMediaPlayer.add(info.activityInfo.packageName);
        }
        Hashtable<String, String> activeMediaProcesses=getActiveProcesses(listMediaPlayer);
        Set<String> activeProcesses=activeMediaProcesses.keySet();
        for (String pkg:activeProcesses)
        {

            if(activeMediaProcesses.get(pkg).contains(action))
                currentPackageNamePlaying = action;

        }



    }




    private boolean isPackageInActiveProcesses(String pack,
                                               List<String> packagesNamesOfAllMediaServices) {

        for (int i = 0; i < packagesNamesOfAllMediaServices.size(); i++)
            if (pack.contains(packagesNamesOfAllMediaServices.get(i)))
                return true;
        return false;
    }
    private Hashtable<String,String> getActiveProcesses(
            List<String> packagesNamesOfAllMediaServices) {
        Hashtable<String, String> componentsRunning = new Hashtable<String, String>();
        ActivityManager am = (ActivityManager) this
                .getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningServiceInfo> rs = am.getRunningServices(50);
        for (int i = 0; i < rs.size(); i++) {
            ActivityManager.RunningServiceInfo rsi = rs.get(i);
            if (isPackageInActiveProcesses(rsi.process,
                    packagesNamesOfAllMediaServices)) {
                LogUtils.logInfo("ActiveProcesses", rsi.process + "\n  " + rsi.service.getClassName()
                                + " pid:" + rsi.pid + "last activiy time:" + rsi.lastActivityTime
                );
                componentsRunning.put(rsi.process, rsi.service.getClassName());

            }



        }

        // removing lyrics services
        for (int i = 0; i < componentsRunning.size(); i++)
            for (int j = 0; j < blackListOfLyricsServices.size(); j++)
                if (componentsRunning.containsKey(blackListOfLyricsServices
                        .get(j)))
                    componentsRunning.remove(blackListOfLyricsServices.get(j));
        return componentsRunning;
    }



    public static List<String> getHiddenTexts(Notification notification)
    {
        // We have to extract the information from the view
        RemoteViews views = notification.bigContentView;
        if (views == null) views = notification.contentView;
        if (views == null) return null;

        // Use reflection to examine the m_actions member of the given RemoteViews object.
        // It's not pretty, but it works.
        List<String> text = new ArrayList<String>();
        try
        {
            Field field = views.getClass().getDeclaredField("mActions");
            field.setAccessible(true);

            @SuppressWarnings("unchecked")
            ArrayList<Parcelable> actions = (ArrayList<Parcelable>) field.get(views);

            // Find the setText() and setTime() reflection actions
            for (Parcelable p : actions)
            {
                Parcel parcel = Parcel.obtain();
                p.writeToParcel(parcel, 0);
                parcel.setDataPosition(0);

                // The tag tells which type of action it is (2 is ReflectionAction, from the source)
                int tag = parcel.readInt();
                if (tag != 2) continue;

                // View ID
                parcel.readInt();

                String methodName = parcel.readString();
                if (methodName == null) continue;

                    // Save strings
                else if (methodName.equals("setText"))
                {
                    // Parameter type (10 = Character Sequence)
                    parcel.readInt();

                    // Store the actual string
                    String t = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel).toString().trim();
                    text.add(t);
                }

                // Save times. Comment this section out if the notification time isn't important
                else if (methodName.equals("setTime"))
                {
                    // Parameter type (5 = Long)
                    parcel.readInt();

                    String t = new SimpleDateFormat("h:mm a").format(new Date(parcel.readLong()));
                    text.add(t);
                }

                parcel.recycle();
            }
        }

        // It's not usually good style to do this, but then again, neither is the use of reflection...
        catch (Exception e)
        {
            LogUtils.logError("NotificationClassifier", e.toString());
        }

        return text;
    }
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {

        try {
            System.out.println("onAccessibilityEvent");
            LogUtils.logInfo(TAG, "notification: " + event.getText());
            MyLibTablesDef.init(this);
            Parcelable data = event.getParcelableData();
            if (data instanceof Notification) {
                Notification noti = (Notification) data;
                List<String> texts = getHiddenTexts(noti);
                for (String hiddenText:texts)
                    Log.i(TAG,"HiddenText:"+hiddenText);
                Bundle bund = noti.extras;
                //Set<String> keys = bund.keySet();
                String pkgName = event.getPackageName().toString();
              //  logEvent(event, noti, bund, pkgName);
                Bundle bundleOut = new Bundle();
                Context con = getBaseContext();

                Intent newIntent = extractMetadataFromNotification(bund, pkgName, bundleOut, con, texts);
                if (newIntent != null) {
                    bundleOut.putString(MyMediaMReceiver.DATA_KEY_ACTION, pkgName);
                    newIntent.setAction(FansinoIntentService.ACTION_METADATA);
                    newIntent.putExtras(bundleOut);
                    con.startService(newIntent);
                }

            }
        }catch (IllegalAccessError e)
        {
            //hotfix - to change later all services to use hiddenNotificationsTexts

        }
        catch(Exception e)
        {
            FansinoSdk.logException(e);
        }
    }


    private Bundle getExtras(Notification notification) {
        try {
            Field field = Notification.class.getDeclaredField("extras");
            field.setAccessible(true);
            return (Bundle) field.get(notification);
        } catch (Exception e) {
            Log.w(TAG, "Failed to access extras on Jelly Bean.");
            return null;
        }
    }



    public static Intent extractMetadataFromNotification(Bundle bund, String pkgName, Bundle bundleOut, Context con,List<String> hiddenNotificationsTexts) {
        Intent newIntent = new Intent(con, FansinoIntentService.class);
        boolean parsedCompletedWithoutError=false;
        try {
            LogUtils.logInfo(TAG, "bundle infotext: " + bund.getString(EXTRA_INFO_TEXT));
            LogUtils.logInfo(TAG, "bundle text: " + bund.getString(EXTRA_TEXT));
            LogUtils.logInfo(TAG, "bundle title: " + bund.getString(EXTRA_TITLE));
            LogUtils.logInfo(TAG, "bundle sub text: " + bund.getString(EXTRA_SUB_TEXT));
            LogUtils.logInfo(TAG, "package name: " + pkgName);



        } catch (Exception e)
        {}


        try {
            bundleOut.putString(MyMediaMReceiver.DATA_KEY_ACTION, pkgName);
            bundleOut.putString("album", "UNKNOWN");

            bundleOut.putBoolean(IS_FROM_ACCESSIBILITY, true);

            if (pkgName.compareTo(Pandora.PKG_NAME) == 0) {

                parsedCompletedWithoutError=Pandora.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(MixCloud.PKG_NAME) == 0) {

                parsedCompletedWithoutError= MixCloud.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(Spotify.PKG_NAME) == 0) {

                parsedCompletedWithoutError= Spotify.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(BeatsMusic.PKG_NAME) == 0) {

                parsedCompletedWithoutError=BeatsMusic.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(SlackerRadio.PKG_NAME) == 0) {

                parsedCompletedWithoutError= SlackerRadio.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(Rdio.PKG_NAME) == 0) {

                parsedCompletedWithoutError= Rdio.parse(bundleOut, hiddenNotificationsTexts);


            } else if (pkgName.compareTo(IHeartRadio.PKG_NAME) == 0) {

                parsedCompletedWithoutError= IHeartRadio.parse(bundleOut, hiddenNotificationsTexts);


            } else if (pkgName.compareTo(Songza.PKG_NAME) == 0) {

                parsedCompletedWithoutError= Songza.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(Seriusxm.PKG_NAME) == 0) {

                parsedCompletedWithoutError= Seriusxm.parse(bundleOut, hiddenNotificationsTexts);


            } else if (pkgName.compareTo(Tunein.PKG_NAME) == 0) {
                parsedCompletedWithoutError= Tunein.parse(bundleOut, hiddenNotificationsTexts);

            } else if (pkgName.compareTo(SonyMusic.PKG_NAME) == 0) {
                parsedCompletedWithoutError= SonyMusic.parse(bundleOut, hiddenNotificationsTexts);


            } else if (pkgName.compareTo(Bandcamp.PKG_NAME) == 0) {

                parsedCompletedWithoutError=  Bandcamp.parse(bundleOut, hiddenNotificationsTexts);


            } else if (pkgName.compareTo(JangoRadio.PKG_NAME)==0)
                parsedCompletedWithoutError=  JangoRadio.parse(bundleOut, hiddenNotificationsTexts);
            else if (pkgName.compareTo(EightTracks.PKG_NAME)==0)
                parsedCompletedWithoutError=  EightTracks.parse(bundleOut, hiddenNotificationsTexts);

            else if (pkgName.compareTo(YouTube.PKG_NAME)==0)
                parsedCompletedWithoutError=YouTube.parse(bundleOut, hiddenNotificationsTexts);



            else {
                // performAppRecognition(pkgName);
                return null;

            }
        }
        catch(Exception e)
        {LogUtils.logError(TAG,"extractMetadataFromNotification",e);
            return null;
        }
        if (parsedCompletedWithoutError)
            return newIntent;
        else
            return null;

    }



    private void logEvent(AccessibilityEvent event, Notification noti,Bundle bund,String pkgName) {
        Set<String> keys=bund.keySet();

        Bundle bundle = new Bundle();
        Intent newIntent = new  Intent("omz.fansino.fansinolib.NEW_AUDIO_METADATA_NOTIFICATION");
        LogUtils.logInfo(TAG, "**********  onNotificationPosted keys set:");
        for( String key:keys)
            LogUtils.logInfo(TAG, "**********key:" + key + "=" + bund.get(key));
        LogUtils.logInfo(TAG, "**********  pkgName:" + pkgName);
        LogUtils.logInfo(TAG, "**********sbn event before text:" + event.getBeforeText());
        LogUtils.logInfo(TAG, "**********sbn event contentDescription:" + event.getContentDescription());
        if (noti.tickerText!=null)
            LogUtils.logInfo(TAG, "**********sbn contentView tickerText:" + noti.tickerText);
    }

    @Override
    protected void onServiceConnected() {
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.eventTypes = AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        info.notificationTimeout = 100;

        setServiceInfo(info);
    }

    @Override
    public void onInterrupt() {
        System.out.println("onInterrupt");
    }
}