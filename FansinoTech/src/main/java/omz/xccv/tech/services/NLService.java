package omz.xccv.tech.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import java.util.List;

import omz.xccv.tech.receivers.MyMediaMReceiver;
import omz.xccv.tech.utils.LogUtils;

public class NLService extends NotificationListenerService {

    private String TAG = this.getClass().getSimpleName();
    private NLServiceReceiver nlservicereciver;
    @Override
    public void onCreate() {
        super.onCreate();
        nlservicereciver = new NLServiceReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.kpbird.nlsexample.NOTIFICATION_LISTENER_SERVICE_EXAMPLE");
        registerReceiver(nlservicereciver,filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nlservicereciver);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {


        try {
            Log.i(TAG, "**********  onNotificationPosted");
            Log.i(TAG, "ID :" + sbn.getId() + "t" + sbn.getNotification().tickerText + "t" + sbn.getPackageName());
            Bundle bundleOut = new Bundle();
            Bundle bundleIn = sbn.getNotification().extras;
            String packageName = sbn.getPackageName();
            List<String> hiddenTexts = NotificationAccessibilityService.getHiddenTexts(sbn.getNotification());
            if (hiddenTexts.size() == 0) {
                for (String key : bundleIn.keySet()) {
                    if ((bundleIn.get(key) instanceof String) && (bundleIn.getString(key) != null))
                        hiddenTexts.add(bundleIn.getString(key));
                }
            }
            for (String hiddenText : hiddenTexts)
                Log.i(TAG, "HiddenText:" + hiddenText);

            Intent newIntent = NotificationAccessibilityService.extractMetadataFromNotification(bundleIn, packageName, bundleOut, getApplicationContext(), hiddenTexts);

            if (newIntent != null) {
                bundleOut.putString(MyMediaMReceiver.DATA_KEY_ACTION, sbn.getPackageName());
                newIntent.setAction(FansinoIntentService.ACTION_METADATA);
                newIntent.putExtras(bundleOut);
                getApplicationContext().startService(newIntent);
            }
        }catch(Exception e)
        {
            LogUtils.logError(TAG,"onNotificationPosted",e);
        }








    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {


    }

    class NLServiceReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getStringExtra("command").equals("clearall")){
                NLService.this.cancelAllNotifications();
            }
            else if(intent.getStringExtra("command").equals("list")){
                Intent i1 = new  Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
                i1.putExtra("notification_event","=====================");
                sendBroadcast(i1);
                int i=1;
                for (StatusBarNotification sbn : NLService.this.getActiveNotifications()) {
                    Intent i2 = new  Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
                    i2.putExtra("notification_event",i +" " + sbn.getPackageName() + "n");
                    sendBroadcast(i2);
                    i++;
                }
                Intent i3 = new  Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_EXAMPLE");
                i3.putExtra("notification_event","===== Notification List ====");
                sendBroadcast(i3);

            }

        }
    }

}