package omz.xccv.tech.services;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

import com.gracenote.mmid.MobileSDK.GNSearchResponse;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import omz.apps.magnetmessage.manager.LoginManager;
import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.apis.EchoNestApi;
import omz.xccv.tech.apis.GraceNoteApi;
import omz.xccv.tech.constants.TimeConstants;
import omz.xccv.tech.interfaces.EventConsumer;
import omz.xccv.tech.interfaces.MetadataLocalDBCallbacks;
import omz.xccv.tech.managers.AnalyticsManager;
import omz.xccv.tech.models.ActiveMediaProcess;
import omz.xccv.tech.models.Metadata;
import omz.xccv.tech.receivers.MyMediaMReceiver;
import omz.xccv.tech.utils.FansinoTechApi;
import omz.xccv.tech.utils.LogUtils;
import omz.xccv.tech.utils.SerializationObjectsUtil;
import omz.xccv.tech.utils.UserData;
import omz.xccv.tech.utils.UserProfile;
import omz.xccv.tech.utils.Utils;

//import com.gracenote.mmid.MobileSDK.GNSearchResponse;

public class FansinoIntentService extends IntentService implements EventConsumer
		 {

	private static final String SRV_PARAM_KEY_ARTISTS_AND_PLAY_TIMES = "artistsAndPlayTimes";
	private static final String FANSINOSERVER_CALC_FAN_RANK_PATH = "http://fansinoserver.appspot.com/CalcFanRank";
	public static final String ACTION_METADATA = "omz.fansino.fan.newmetadata";
    public static final String ACTION_FETCH_RANKS = "omz.fansino.fan.fetchranks";
	public static final String ACTION_ANALZIE_LOCAL_MEDIA = "omz.fansino.fan.anallocaldata";
    public static final String ACTION_REGISTER_USER = "omz.xccv.tech.register";
    public static final String ACTION_TRY_TO_RECOGNIZE_FROM_MIC = "omz.xccv.tech.audio.recognize.mic";

    public static final String INPUT_ARG_ARTISTS_AND_PLAYING_TIMES = "artistsAndPlayingTimes";
	public static final String OUTPUT_ARG_ARTISTS_AND_RANKS = "artistsAndRanks";
    public static final String JSON_KEY_GCM_MESSAGE_TYPE = "MT";
	private static final long TIME_INTERVAL_FOR_CHECKING_WHEN_IS_THE_LAST_SONG_HEARD = TimeConstants.MINUTE*5;
	private static final String TAG = "FansinoIntentService";
	private static Metadata oldMetadata;
	private static Metadata currentMetadata;
	private static Timer checkWhenLastSongWasHeardTimer;
	private static boolean fanisStillListeningListening = false;
	//private static GraceNoteApi graceNoteApi;
    private static boolean isNotFirstMetadataIntent=false;
    private static GraceNoteApi gracenoteApi;

             public FansinoIntentService() {
                 super(TAG);
             }



	@Override
	public void onCreate() {
		// TODO Auto-generated method stub

		super.onCreate();
		LogUtils.logInfo("FANSINO SERVER", "ON CREATE");

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		LogUtils.logInfo("FANSINO SERVER", "START");
		return super.onStartCommand(intent, flags, Service.START_STICKY);

	}

	@Override
	public void event(int eventId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void event(int eventId, Object data) {
		// TODO Auto-generated method stub

		switch (eventId) {

		case GraceNoteApi.EVENT_ID_SONG_RECONGNIZED_FROM_FILE:
            LogUtils.logDebug(TAG,"EVENT_ID_SONG_RECONGNIZED_FROM_FILE");

			break;
		case GraceNoteApi.EVENT_ID_SONG_RECONGNIZED_FROM_MIC:



                LogUtils.logDebug(TAG, "EVENT_ID_SONG_RECONGNIZED_FROM_MIC");

                if (data != null) {

                    final GNSearchResponse response = (GNSearchResponse) data;

                    Thread thread=new Thread(){

                        public void run()
                        {
                            try {
                            String service = "un.un.un";

                            Bundle bundle = new Bundle();

                            bundle.putString("artist", response.getArtist());
                            bundle.putString("album", response.getAlbumTitle());
                            bundle.putString("track", response.getTrackTitle());
                            bundle.putString(MyMediaMReceiver.DATA_KEY_ACTION, service);
                            handleMetadata("com.android.music.metachanged", bundle);
                            }catch(Exception e)
                            {
                                FansinoSdk.logException(e);
                            }
                        }
                    };
                    thread.start();

                }

			break;
		case GraceNoteApi.EVENT_ID_SONG_NOT_RECONGNIZED_FROM_FILE:
            LogUtils.logDebug(TAG,"EVENT_ID_SONG_NOT_RECONGNIZED_FROM_FILE");

			break;
		case GraceNoteApi.EVENT_ID_SONG_NOT_RECONGNIZED_FROM_MIC:
            LogUtils.logDebug(TAG,"EVENT_ID_SONG_NOT_RECONGNIZED_FROM_MIC");

			break;
		}
	}

	@Override
	public void event(int eventId, Object[] data) {
		// TODO Auto-generated method stub

	}



    private void audioManagerReflection()
    {
        AudioManager audioManager=(AudioManager)getBaseContext().getSystemService(Context.AUDIO_SERVICE);
        String sampleRate=audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
        audioManager.getClass();
        String sClassName = "android.media.AudioManager";
        try {
            Class classToInvestigate = Class.forName(sClassName);
            Field[] aClassFields = classToInvestigate.getDeclaredFields();
            for(Field f : aClassFields){
                // Found a field f
                //Log.d(TAG,f.toString());
            }

            // Dynamically do stuff with this class
            // List constructors, fields, methods, etc.

        } catch (ClassNotFoundException e) {
            // Class not found!
        } catch (Exception e) {
            // Unknown exception
        }

    }

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub


           // FansinoSdk.auth(this);


        try {
            Utils.setContext(getApplicationContext());
			if (intent.getAction() == ACTION_METADATA&&isNotFirstMetadataIntent) {
				Bundle bundle = intent.getExtras();
				String action = bundle
						.getString(MyMediaMReceiver.DATA_KEY_ACTION);
			//	String[] keys = bundle
			//			.getStringArray(MyMediaMReceiver.DATA_KEY_KEY_SET);

               // audioManagerReflection();

				handleMetadata(action, bundle);



			} else if (intent.getAction() == ACTION_FETCH_RANKS) {

				String artistsAndRanks = requestRanksFromServer(intent
						.getStringExtra(INPUT_ARG_ARTISTS_AND_PLAYING_TIMES));
				Intent intentResponse = new Intent();
				intentResponse.setAction(ACTION_FETCH_RANKS);
				intentResponse.putExtra(OUTPUT_ARG_ARTISTS_AND_RANKS,
						artistsAndRanks);
				LocalBroadcastManager.getInstance(getApplicationContext())
						.sendBroadcast(intentResponse);

			} else if (intent.getAction() == ACTION_ANALZIE_LOCAL_MEDIA) {



            }

            else if (intent.getAction() == ACTION_REGISTER_USER) {

              UserData userData= (UserData) intent.getExtras().getSerializable(UserData.PREFS_KEY_USER_PROFILE);
              if (userData.imageUrl.compareTo(UserProfile.KEY_DATA_UNKNOWN)==0)
              {
                  EchoNestApi echonestApi=new EchoNestApi();
                  userData.imageUrl=echonestApi.getArtistImage(userData.displayName);
                  userData.updateInPrefs(getBaseContext());
                  userData.updateUserProfileImageInPrefs(getBaseContext());
                  userData.updateUserNameInPrefs(getBaseContext());


              }
                UserProfile profile=new UserProfile(userData, this.getBaseContext());
                profile.whenLoginComplete(getApplicationContext());
               // FansinoSdk.initSemaphoreForActionsRequireUserId();


                String userName=userData.validatedId;
                String password=userData.validatedId+userData.firstName+userData.lastName;

                LoginManager.getInsance().attemptLogin(getApplicationContext(), userName, password, true);

        }
            else if(intent.getAction() == ACTION_TRY_TO_RECOGNIZE_FROM_MIC)
            {

                LogUtils.logDebug(TAG,"ACTION_TRY_TO_RECOGNIZE_FROM_MIC");
                if (gracenoteApi==null)
                   gracenoteApi= new GraceNoteApi(getApplicationContext(),this);
               gracenoteApi.recognizeAudioFromMic();
            }



            isNotFirstMetadataIntent=true;
		} catch (Exception e) {
			FansinoSdk.logException(e);
		}

	}




	private String requestRanksFromServer(String artistsAndPlayTimes) {

		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(FANSINOSERVER_CALC_FAN_RANK_PATH);

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair(
					SRV_PARAM_KEY_ARTISTS_AND_PLAY_TIMES, artistsAndPlayTimes));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			String responseStr = EntityUtils.toString(response.getEntity());
			return responseStr;
		} catch (ClientProtocolException e) {
			FansinoSdk.logException(e);
			// TODO Auto-generated catch block
		} catch (IOException e) {
			FansinoSdk.logException(e);
			// TODO Auto-generated catch block
		}
		return null;

	}

             private String performAppRecognitionOmriTry() {

                 /**
                  * Attempt 6: PackageManager + queryBroadcastReceiver 2
                  */
                 final PackageManager pm = getPackageManager();
                 Intent intentToCheck = new Intent(Intent.ACTION_MEDIA_BUTTON);
                 List<ResolveInfo> listCheckedIntent = pm.queryBroadcastReceivers(
                         intentToCheck, 0);
                 List<String> listMediaPlayer = new ArrayList<String>();
                 for (ResolveInfo info : listCheckedIntent) {
                     listMediaPlayer.add(info.activityInfo.packageName);
                 }
                 String packageName=getActiveProcessesRunningServiceInfo(listMediaPlayer);
                 return packageName;

             }

             private String  getActiveProcessesRunningServiceInfo(
                     List<String> packagesNamesOfAllMediaServices) {
                 ActivityManager am = (ActivityManager) this
                         .getSystemService(ACTIVITY_SERVICE);
                 List<ActiveMediaProcess> rsMedia=new ArrayList<ActiveMediaProcess>();
                 List<ActivityManager.RunningServiceInfo> rs = am.getRunningServices(100);
                 List<ActivityManager.RunningAppProcessInfo> appProcesses = am.getRunningAppProcesses();
                 appProcesses.size();


                 List<ActivityManager.RunningServiceInfo> services=new ArrayList<ActivityManager.RunningServiceInfo>();
                 List<ActivityManager.RunningAppProcessInfo> musicAppProcessed=new ArrayList<ActivityManager.RunningAppProcessInfo>();

                 for(int i=0;i<appProcesses.size();i++)
                 {
                     if(isPackageInActiveProcesses(appProcesses.get(i).processName,
                             packagesNamesOfAllMediaServices)&&appProcesses.get(i).importance<300)
                      musicAppProcessed.add(appProcesses.get(i));

                 }
                 if (musicAppProcessed.size()==0)
                      return  "un.un.un";
                 else
                 if( musicAppProcessed.size()==1)
                     return musicAppProcessed.get(0).pkgList[0];
                 else
                 {
                    if( musicAppProcessed.get(0).importance==100)
                        return musicAppProcessed.get(0).pkgList[0];
                    else if (musicAppProcessed.get(0).importance==130)
                        return musicAppProcessed.get(0).pkgList[0];


                 }

                 return  "un.un.un";
             }


             private List<ActivityManager.RunningAppProcessInfo> getAppProcessWithSamePidAsProcesses(List<ActiveMediaProcess> mediaProcesses,List<ActivityManager.RunningAppProcessInfo> musicAppProcesses)
             {
                 List<ActivityManager.RunningAppProcessInfo> activeMusicApps=new ArrayList<ActivityManager.RunningAppProcessInfo>();
                 for (ActivityManager.RunningAppProcessInfo appProcess: musicAppProcesses)
                 {
                     for (ActiveMediaProcess activeProcess:mediaProcesses) {
                         if (appProcess.uid == activeProcess.uid) {
                             activeMusicApps.add(appProcess);
                             break;
                         }

                     }
                 }

                 return activeMusicApps;


             }

             private boolean isPackageInActiveProcesses(String pack,
                                                        List<String> packagesNamesOfAllMediaServices) {

                 for (int i = 0; i < packagesNamesOfAllMediaServices.size(); i++)
                     if (pack.contains(packagesNamesOfAllMediaServices.get(i)))
                         return true;
                 return false;
             }

	private int handleMetadata(String action, Bundle bundle)
			throws JSONException, CloneNotSupportedException {


        String artist = "";
        String album = "";
        String track = "";
        long trackID = 1;
        String serviceName=action;
        int serviceId;
        int userId = Utils.getIntFromPrefs(Utils.PREFS_KEY_USER_ID);
        if( userId==0)
        {
            //retrying sending users data to register in server
            try {
                String regGcmId = Utils.getGCMPreferences(getApplicationContext()).getString(Utils.PROPERTY_REG_ID, "");
                SharedPreferences mPreferences = PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext());
                UserData userData = (UserData) SerializationObjectsUtil.stringToObject(mPreferences.getString(UserData.PREFS_KEY_USER_PROFILE, ""));
                FansinoTechApi sts = new FansinoTechApi(getApplicationContext());
                sts.insertUserSync(mPreferences.getString(UserData.PREFS_KEY_ROLE, ""), userData.displayName, userData.country, userData.gender, 0, 0, 0, regGcmId, userData.validatedId, userData.imageUrl,
                        userData.email, userData.birthday, userData.city, null,getApplicationContext());
                userId=Utils.getIntFromPrefs(Utils.PREFS_KEY_USER_ID);

            }catch(Exception e)
            {
                LogUtils.logError(TAG,"trying to register user to server",e);
            }
        }

        long currentListenedTimeInMilli = Calendar.getInstance()
                .getTimeInMillis();
        //String serviceName = performAppRecognition(action);
        if (action != null) {
            if (action.compareTo("com.amazon.mp3.metachanged") == 0) {
                trackID = (long) bundle.getLong("com.amazon.mp3.id");
                artist = bundle.getString("com.amazon.mp3.artist");
                album = bundle.getString("com.amazon.mp3.album");
                track = bundle.getString("com.amazon.mp3.track");

            }else if (action.compareTo("com.android.music.playstatechanged")==0)
                return 2;


            else {
                //trackID = (long) bundle.getLong("id");



                artist = bundle.getString("artist");
                album = bundle.getString("album");
                track = bundle.getString("track");
                if (bundle.containsKey(MyMediaMReceiver.DATA_KEY_ACTION)) {
                    if (bundle.getString(MyMediaMReceiver.DATA_KEY_ACTION)=="un.un.un");//FROM GRACENOTE AUDIO RECOGNITION

                    else if (!bundle.containsKey(NotificationAccessibilityService.IS_FROM_ACCESSIBILITY))
                        serviceName = performAppRecognitionOmriTry();

                    else{
                        String serviceNameFromAccessbility = bundle.getString(MyMediaMReceiver.DATA_KEY_ACTION);
                        if (serviceNameFromAccessbility != null)
                            serviceName = serviceNameFromAccessbility;
                    }
                }
            }
            serviceId = FansinoSdk.recognizeMusicService(getBaseContext(),serviceName);

            if (checkWhenLastSongWasHeardTimer == null) {
                checkWhenLastSongWasHeardTimer = new Timer();
                checkWhenLastSongWasHeardTimer.schedule(new CheckIfUserStoppedListeningToMusic(), 0, 300);
            }

            if (trackID >= 0)
                currentMetadata = new Metadata(artist, album, track, serviceName,
                        trackID, currentListenedTimeInMilli,serviceId,
                        myMetadataServerCallBacksInterface,getApplicationContext());
            fanisStillListeningListening = true;
            if (currentMetadata != null && !currentMetadata.compareTo(oldMetadata)) {
              //  Toast.makeText(
              //          FansinoSdk.mActivityContext,keys.length+"\t"+
              //          trackID + "\t" + artist + "\t" + album + "\t" + track
              //                  + "\n" + serviceName, Toast.LENGTH_LONG
            //    ).show();
                AnalyticsManager.sendEvent("listening to",artist);


                currentMetadata.insertMusicMeatadata(getContentResolver());
                LogUtils.logInfo(TAG, "userId:" + userId);
                if (oldMetadata != null) {
                    oldMetadata.playTimeInSeconds = ((currentListenedTimeInMilli-oldMetadata.whenPlayedInMilli)/1000);
                    oldMetadata
                            .updateMusicMeatadataPLayingTime(getContentResolver());
                    oldMetadata
                            .updateMusicMeatadataNotPlayingNow(getContentResolver());
                    oldMetadata.metadataUpdateState(
                            Metadata.METADATA_STATE_NOT_UPDATED,
                            getContentResolver());
                    oldMetadata.updatePlayTimeMetadata(userId,
                                myMetadataServerCallBacksInterface);
                }
                if (userId >= 0) {
                    currentMetadata.sendMetadata(userId,
                            myMetadataServerCallBacksInterface,
                            getContentResolver());
                }
                oldMetadata = (Metadata) currentMetadata.clone();
            }
        }


        return 1;
	}

	class CheckIfUserStoppedListeningToMusic extends TimerTask

	{

		@Override
		public void run() {
            // TODO Auto-generated method stub
            if (currentMetadata != null) {
                long DeltaBetweenNowAndLastStartedSongTimeInMilli = Calendar
                        .getInstance().getTimeInMillis()
                        - FansinoIntentService.currentMetadata.whenPlayedInMilli;
                if (fanisStillListeningListening&&DeltaBetweenNowAndLastStartedSongTimeInMilli > FansinoIntentService.TIME_INTERVAL_FOR_CHECKING_WHEN_IS_THE_LAST_SONG_HEARD) {
                    fanisStillListeningListening = false;
                    FansinoIntentService.currentMetadata.updateFanStopedListening(Utils.getIntFromPrefs(Utils.PREFS_KEY_USER_ID));
                    checkWhenLastSongWasHeardTimer.cancel();
                    checkWhenLastSongWasHeardTimer = null;


                } else
                    fanisStillListeningListening = true;

            }
        }

	}

	MetadataLocalDBCallbacks myMetadataServerCallBacksInterface = new MetadataLocalDBCallbacks() {

		@Override
		public void metadataSentToServer(Metadata metadata) {
			// TODO Auto-generated method stub
			LogUtils.logInfo("MetadataProcessing", "metadataSentToServer:" + metadata);

			metadata.metadataUpdateState(Metadata.METADATA_STATE_SENT,
					getContentResolver());

		}

		@Override
		public void metadataNotSentToServer(Metadata metadata, Exception e) {
			// TODO Auto-generated method stub
			LogUtils.logError("MetadataProcessing", "metadataNotSentToServer:" + metadata);
		}

		@Override
		public void metadataUpdatedInServer(Metadata metadata) {
			// TODO Auto-generated method stub
			LogUtils.logInfo("MetadataProcessing", "metadataUpdatedInServer:" + metadata);

			metadata.metadataUpdateState(Metadata.METADATA_STATE_UPDATED,
					getContentResolver());

		}

		@Override
		public void metadataNotUpdatedInServer(Metadata metadata, Exception e) {
			// TODO Auto-generated method stub
            LogUtils.logError("MetadataProcessing", "metadataNotUpdatedInServer:"
					+ metadata);

		}

		@Override
		public void insertMetadataComplete(Uri uri) {
			// TODO Auto-generated method stub
			LogUtils.logInfo("MetadataProcessing", "insertMetadataComplete:" + uri);

		}

		@Override
		public void updateMetadataNotPlayingNowComplete(Uri uri) {
			// TODO Auto-generated method stub
			LogUtils.logInfo("MetadataProcessing", "updateMetadataNotPlayingNowComplete:"
                    + uri);

		}

		@Override
		public void updateMetadataPlayingTime(Uri uri) {
			// TODO Auto-generated method stub
			LogUtils.logInfo("MetadataProcessing", "updateMetadataPlayingTime:" + uri);

		}

	};

}