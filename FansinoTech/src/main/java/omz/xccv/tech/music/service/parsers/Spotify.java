package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class Spotify {
    public static final String PKG_NAME = "com.spotify.music";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {
        String hidden1 = hiddenNotificationsTexts.get(1);
        if (!hidden1.isEmpty()) {
            bundleOut.putString("track", hiddenNotificationsTexts.get(0));
            bundleOut.putString("album", hidden1);
            bundleOut.putString("artist", hiddenNotificationsTexts.get(2));
            return true;

        }else
            return false;


    }
}
