package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

import omz.xccv.tech.utils.LogUtils;

/**
 * Created by omrierez on 12/10/14.
 */
public class YouTube {
    public static final String PKG_NAME="com.google.android.youtube";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {
       try {
           String[] hidden = hiddenNotificationsTexts.get(0).split("-");
           String track = "";
           String artist = hidden[0];
           for (int i = 0; i < hidden.length; i++) {
               if (i == 0)
                   artist = hidden[i].trim();
               else
                   track += hidden[i] + " ";
           }

           bundleOut.putString("track", track);
           bundleOut.putString("artist", artist);
           return true;

       }catch(Exception e)
       {
           LogUtils.logError(PKG_NAME,"Error in parsing matadata",e);
       }
        return false;
    }
}
