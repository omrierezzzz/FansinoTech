package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class BeatsMusic {
    public static final String PKG_NAME = "com.beatsmusic.android.client";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {

        String track = hiddenNotificationsTexts.get(0);
        String album = hiddenNotificationsTexts.get(1);
        String artist = hiddenNotificationsTexts.get(2);
        if (artist.compareTo("AD") == 0)
            return false;
        bundleOut.putString("track", track);
        bundleOut.putString("artist", artist);
        bundleOut.putString("album", album);

        return true;
    }
}
