package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class SlackerRadio {//USA, Canada
    public static final String PKG_NAME="com.slacker.radio";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {

        String text1 = hiddenNotificationsTexts.get(0);
        String text2;
        try {
            text2 = hiddenNotificationsTexts.get(1);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        if (text2.compareTo("AD") == 0)
            return false;
        bundleOut.putString("track", text1);
        bundleOut.putString("artist", text2);
        return true;
    }
}
