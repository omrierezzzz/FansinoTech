package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class EightTracks {
    public static final String PKG_NAME = "com.e8tracks";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {
        int titleIndex=hiddenNotificationsTexts.size()-2;
        int artistIndex=hiddenNotificationsTexts.size()-1;
        String artist = hiddenNotificationsTexts.get(artistIndex);
        String title = hiddenNotificationsTexts.get(titleIndex);
        bundleOut.putString("track", title);
        bundleOut.putString("artist", artist);
        return true;
    }
}
