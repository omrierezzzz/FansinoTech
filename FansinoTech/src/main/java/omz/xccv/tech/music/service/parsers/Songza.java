package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class Songza {
    public static final String PKG_NAME = "com.ad60.songza";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {

        String track = hiddenNotificationsTexts.get(1);
        String artist = hiddenNotificationsTexts.get(0);
        if (artist.contains("Getting next song") || artist.compareTo("We're so sorry, but our music licenses only allow us to stream in the US and Canada.") == 0)
            return false;
        bundleOut.putString("track", track);
        bundleOut.putString("artist", artist);
        return true;
    }
}
