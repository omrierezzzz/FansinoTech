package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class IHeartRadio {
    public static final String PKG_NAME = "com.clearchannel.iheartradio.controller";

    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {


        String text1,text2;
        if (hiddenNotificationsTexts.size()==1)
            //radio stations with no metadata of playing song
            return false;

        if (hiddenNotificationsTexts.size()==3) {
            text1 = hiddenNotificationsTexts.get(1);
            text2 = hiddenNotificationsTexts.get(2);
            if (text2.compareTo("AD") == 0)
                return false;
            bundleOut.putString("track", text1);
            bundleOut.putString("artist", text2);
            return true;
        }
        return false;
    }
}
