package omz.xccv.tech.music.service.parsers;

import android.os.Bundle;

import java.util.List;

/**
 * Created by omrierez on 12/10/14.
 */
public class Tunein {


    public static final String PKG_NAME="tunein.player";




    public static boolean parse(Bundle bundleOut, List<String> hiddenNotificationsTexts) {
        String hiddenText=hiddenNotificationsTexts.get(0);
        String station=hiddenNotificationsTexts.get(1);
        //String title = bund.getString(EXTRA_TITLE);
        //String text = bund.getString(EXTRA_TEXT);

        if (hiddenText != null) {
            if (station.contains("Opening") || station.contains("buffered") || station.contains("Fetching playlist") || station.contains("Commercial Break")||station.contains("Getting playlist") )
                return false;
            if (hiddenText.contains("-")) {
                String[] hiddenTextParsed= hiddenText.split("-");
                bundleOut.putString("artist", hiddenTextParsed[0]);
                bundleOut.putString("track",hiddenTextParsed[1]);
                return true;

            }
        }
        if (station != null) {

            if (station.contains("Opening") || station.contains("buffered") || station.contains("Fetching playlist") || station.contains("Commercial Break")||station.contains("Getting playlist"))
                return false;
            bundleOut.putString("artist", station);
        }

        return false;
    }
}
