package omz.xccv.tech.managers;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import omz.xccv.tech.BuildConfig;
import omz.xccv.tech.R;



/**
 * Created by omrierez on 9/27/14.
 */
public  class AnalyticsManager {

     private static Context appContext;
     private final static String FAN_TALK_ROOM_PREFIX="FAN_TALK_FAN_TALK_";
     private final static String EVENT_CATEGORY_FAN="fan android";
     private final static String EVENT_CATEGORY_ARTIST="artist android";

     public static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();
     private static Tracker tracker;
     private static boolean sendEvents= true;
    private static String events_category="";





    public static void init(Context con,boolean isFan)
    {
        appContext=con;
        if(BuildConfig.DEBUG)
            sendEvents=true;
        else
            sendEvents=true;
        if (isFan)
            events_category=EVENT_CATEGORY_FAN;
        else
            events_category=EVENT_CATEGORY_ARTIST;



    }
    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     * <p/>
     * A single tracker is usually enough for most purposes. In case you do need multiple trackers,
     * storing them all in Application object helps ensure that they are created only once per
     * application instance.
     */
    public  enum TrackerName {
        APP_TRACKER, // Tracker used only in this app.
        GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }





    synchronized static Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(appContext);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.app_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }
    synchronized static Tracker getTracker(Context con,TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {

            GoogleAnalytics analytics = GoogleAnalytics.getInstance(con);
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker)
                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
                    : analytics.newTracker(R.xml.app_tracker);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);
    }

    public static void sendEvent(String action,String label)
    {


        if (AnalyticsManager.sendEvents) {
            tracker = (getTracker(TrackerName.APP_TRACKER));
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(events_category)
                    .setAction(action)
                    .setLabel(label)
                    .setValue(1)
                    .build());
        }

    }



    public static void sendEvent(Context con,String action,String label)
    {

        if (AnalyticsManager.sendEvents) {
            tracker= (getTracker(con,TrackerName.APP_TRACKER));

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(events_category)
                .setAction(action)
                .setLabel(label)
                .setValue(1)
                .build());
    }

    }


    public static void sendEventUserInputRelated(String chatId,String role,String text) {
        if (chatId.contains(FAN_TALK_ROOM_PREFIX))
            sendEvent(text+" in fan talk","");
        else if (chatId.length()==36)
            sendEvent(text+" in chat","");
    }

    public static void sendScreenView(Context con,String screenName) {

        if (AnalyticsManager.sendEvents) {
            tracker = (getTracker(con, TrackerName.APP_TRACKER));
        }
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

}
