package omz.xccv.tech;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.accessibility.AccessibilityManager;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import omz.xccv.tech.interfaces.FansinoTechIncomingMessageListener;
import omz.xccv.tech.providers.MyContentProvider;
import omz.xccv.tech.services.FansinoIntentService;
import omz.xccv.tech.services.NotificationAccessibilityService;
import omz.xccv.tech.utils.LogUtils;
import omz.xccv.tech.utils.UserData;
import omz.xccv.tech.utils.UserProfile;
import omz.xccv.tech.utils.Utils;

/**
 * Created by omrierez on 8/4/14.
 */
public class FansinoSdk {


    public static final String FANSINO_SERVER_PATH_GET_MUSIC_SERVICES_MAPPING = "https://fansinoserver.appspot.com/metadata/services";
    private static final String SCOPE =
            "audience:server:client_id:583645316385-bjead6i5r32di12cr911qvf4hag296kf.apps.googleusercontent.com";
    public static final String PREFS_KEY_TOKEN = "token";
    public static final String PREFS_KEY_TOKEN_EXPRIATION_TIME = "tokenTimeExpires";
    public static final long ONE_HOUR_IN_MILLI = 1000*60*60;
    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String ANDROID = "android";
    public static final String UNKNOWN_SERVICE_PACKAGE = "un.un.un";
    public static final String UNKNOWN = "Unknown";
    public static final String UNKNOWN_SERVICE_STR = "Unknown Service";

    public static String accessToken;
    public class FansinoConfig{

        public static final boolean SEND_EXCEPTIONS_TO_CRASHLYTICS=true;

    }
    public static Context mAppContext;
    public static Context mActivityContext;
    public static final String TAG="FansinoSdk";
    public static final String PREFS_KEY_MUSIC_SERVICES_SUPPORTED="musicServices";
    public static JSONArray musicServicesPackagesToServiceNames;
   /* public static Semaphore actionsRequireUserIdSemaphore=null;


    public static void initSemaphoreForActionsRequireUserId() throws InterruptedException
    {
        actionsRequireUserIdSemaphore=new Semaphore(1);
        actionsRequireUserIdSemaphore.drainPermits();

    }
    public static void releaseSemaphoreForActionsRequireUserId() throws InterruptedException
    {
        actionsRequireUserIdSemaphore.release();

    }
    public static void acquireSemaphoreForActionsRequireUserId() throws InterruptedException
    {
        if (actionsRequireUserIdSemaphore!=null) {
            actionsRequireUserIdSemaphore.acquire();
            actionsRequireUserIdSemaphore=null;

        }
        else
          return;

    }
*/



    public static Map<String,Integer> getMostHeardArtistsFromDb()
    {
        return Utils.sortMapByComparator(MyContentProvider.getMostHeardArtistsFromDb(),true);
    }

    public static void init(Context con,Class classService)
    {
        mActivityContext =con;
        con.startService(new Intent(con,classService));
       // registerMediaButtonReceiver(con);
        //auth(mAppContext);
    }


    public static void auth(final Context con)

    {
        if (con!=null) {
            final SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(con);
            long validationTime=Calendar.getInstance().getTimeInMillis()-ONE_HOUR_IN_MILLI;
            long lastTimeTokenGenerated=prefs.getLong(PREFS_KEY_TOKEN_EXPRIATION_TIME,0);
            if ((lastTimeTokenGenerated<validationTime)) {
                if (accessToken == null || accessToken.isEmpty())
                    new AsyncTask<String, Void, String>() {
                        @Override
                        protected String doInBackground(String... objs) {
                            try {
                                String token="None";
                                String email = UserProfile.getEmailFormGoogleAccount(con);
                                try {
                                    token = GoogleAuthUtil.getToken(con, email, SCOPE);
                                }catch (GooglePlayServicesAvailabilityException e)
                                {}
                                LogUtils.logDebug(TAG, "Token " + token);
                                prefs.edit().putString(PREFS_KEY_TOKEN, token).commit();
                                prefs.edit().putLong(PREFS_KEY_TOKEN_EXPRIATION_TIME, Calendar.getInstance().getTimeInMillis()).commit();

                                accessToken = token;

                            } catch (Exception e) {
                                logException(e);
                            }

                            return null;
                        }


                    }.execute();
            }
            else
               {
                  accessToken= prefs.getString(PREFS_KEY_TOKEN, "");

               }

        }

    }


    public static  void setApplicationContext(Context con)
    {
        mAppContext=con;
    }

    public static void logException(Exception e)
    {
        if(FansinoConfig.SEND_EXCEPTIONS_TO_CRASHLYTICS)
            Crashlytics.logException(e);
        e.printStackTrace();

    }
    public static void setMusicServicesPackagesFromServer(Context con,boolean shouldUpdate)
    {

            String musicServices = Utils.getStringFromPrefs(con,PREFS_KEY_MUSIC_SERVICES_SUPPORTED);
            if (musicServices.isEmpty() || shouldUpdate) {

                String answer = Utils.simpleHttpGet(FANSINO_SERVER_PATH_GET_MUSIC_SERVICES_MAPPING);
                try {
                    musicServicesPackagesToServiceNames = new JSONArray(answer);
                    Utils.setStringInPrefs(con, PREFS_KEY_MUSIC_SERVICES_SUPPORTED, answer);
                } catch (Exception e) {
                    FansinoSdk.logException(e);
                }


            } else try {
                musicServicesPackagesToServiceNames = new JSONArray(musicServices);
            } catch (JSONException e) {
                FansinoSdk.logException(e);
            }

    }
    public static void setMusicServicesPackagesFromServerAsync(final Context con)
    {

        Thread th=new Thread(){

            @Override
            public void run() {
                super.run();

                setMusicServicesPackagesFromServer(con,true);
            }
        };
        th.start();


    }




    public static void registerUser(UserData userData,String role)
    {
        Bundle bundle=new Bundle();
        bundle.putSerializable(UserData.PREFS_KEY_USER_PROFILE,userData);
        SharedPreferences.Editor editor = PreferenceManager
                .getDefaultSharedPreferences(mActivityContext).edit();
        editor.putString(UserData.PREFS_KEY_ROLE,role).commit();
        bundle.putSerializable(UserData.PREFS_KEY_USER_PROFILE,userData);
        Intent intent=new Intent(mActivityContext,FansinoIntentService.class);
        intent.setAction(FansinoIntentService.ACTION_REGISTER_USER);
        intent.putExtras(bundle);
        mActivityContext.startService(intent);

    }


    public static void setFansinoSdkMessageListener(FansinoTechIncomingMessageListener listener)
    {
        GCMService.appMessagesListener=listener;

    }
    public static void showAccessbilityDialog(final Context con)
    {

        if(areThereMusicSerivcesThatRequireAccessibility()&&!isAccessibilityEnabled(con)) {

           final  Dialog dialog = new Dialog(con,R.style.Theme_FansinoBase);

            dialog.getWindow().getAttributes().windowAnimations = R.style.styleDialogSlideAnimation;
            dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_please_turn_on_accessibility);

            (dialog.findViewById(R.id.bOk)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if( Utils.hasSdk20OrHigher())
                        openNotificationsListenerSettings(con);
                    else
                        openAccessibilitySettings(con);
                    if(dialog.isShowing())
                        dialog.dismiss();
                }
            });
            (dialog.findViewById(R.id.bCancel)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                   if(dialog.isShowing())
                      dialog.dismiss();
                }
            });
            dialog.show();

        }


    }

    public static boolean isAccessibilityEnabled(Context context) {

        AccessibilityManager am = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);

        // this can be true even if Addons Detector is on - so is not an indicator if TalkBack is on ..
        boolean isAccessibilityEnabled_flag = am.isEnabled();

        // using these methods which suggested on stackoverflow for checking if TalkBack is on ..
        // this only true if Accessibility TalkBack is on ..


        return isAccessibilityEnabled_flag;

    }

    private static List<String>  returnAllMusicServicesOnDevice(Context con)
    {
         PackageManager pm = con.getPackageManager();
         ActivityManager am = (ActivityManager) con
                .getSystemService(con.ACTIVITY_SERVICE);
        // Check for all media program
        Intent intentToCheck = new Intent(Intent.ACTION_MEDIA_BUTTON);
        List<ResolveInfo> listCheckedIntent = pm.queryBroadcastReceivers(
                intentToCheck, 0);
        List<String> listMediaPlayer = new ArrayList<String>();
        for (ResolveInfo info : listCheckedIntent) {
            listMediaPlayer.add(info.activityInfo.packageName);
        }

        return listMediaPlayer;


    }
    private static void openAccessibilitySettings(Context con)
    {




            Intent sSettingsIntent =
                    new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            con.startActivity(sSettingsIntent);



    }

    private static void openNotificationsListenerSettings(Context con)
    {
        Intent intent=new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
        con.startActivity(intent);


    }

    public static void startStartupActivity(Context con,Intent intent,Class classComp)
    {
        Intent intentStart=new Intent(con, classComp);
        intentStart.putExtras(intent.getExtras());
        intentStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        con.startActivity(intentStart);
    }


    private static String getFrontActivity(Context con)
    {
        ActivityManager am = (ActivityManager) con.getSystemService(con.ACTIVITY_SERVICE);
        // get the info from the currently running task
        List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(1);

        Log.d("topActivity", "CURRENT Activity ::"
                + taskInfo.get(0).topActivity.getClassName());

        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return componentInfo.getPackageName();


    }

    public static String getServiceName(int serviceid)
    {
        try {
            JSONObject obj;
            for (int i=0;i<musicServicesPackagesToServiceNames.length();i++) {
                obj = musicServicesPackagesToServiceNames.getJSONObject(i);
                if (obj.getInt("id")==serviceid)
                    return obj.getString("name");
            }
        }catch(Exception e)
        {


        }

        return UNKNOWN_SERVICE_STR;


    }
    public static int recognizeMusicService(Context con,String intentAction)
    {

        String serviceName=UNKNOWN;
        if( musicServicesPackagesToServiceNames==null) {
            setMusicServicesPackagesFromServer(con,false);
        }
        String frontActivit=getFrontActivity(con);

        String servicePackage= UNKNOWN_SERVICE_PACKAGE;
        JSONObject oneService=null;
        int serviceId=0;
        for (int i=0;i<musicServicesPackagesToServiceNames.length();i++)
        {
            try{

                oneService=musicServicesPackagesToServiceNames.getJSONObject(i);
                servicePackage=oneService.getString(ANDROID);
            }catch(JSONException e){FansinoSdk.logException(e);}

            if (intentAction.compareTo(servicePackage)==0||frontActivit.contains(servicePackage))
            {
                try{
                    serviceName = oneService.getString(NAME);
                    serviceId=oneService.getInt(ID);
                    break;
                }catch( JSONException e)

                {FansinoSdk.logException(e);}
                break;
            }

        }
        if (serviceName.compareTo(UNKNOWN)==0)
        {
            LogUtils.logInfo(TAG, "metadata is played in UNKNOWN SERVICE");

        }

        return serviceId;

    }

    private static boolean areThereMusicSerivcesThatRequireAccessibility( )
    {

        boolean answer=false;
        List<String> musicServices=returnAllMusicServicesOnDevice(mActivityContext);
        for( String  service:musicServices) {
            for (String accessibilityService : NotificationAccessibilityService.MUSIC_SERVICES_WHICH_NEED_ACCESSIBILITY)
                if (service.contains(accessibilityService)) {
                    answer = true;
                    break;
                }
            if (answer==true)
                break;
        }

        LogUtils.logInfo(TAG, "areThereMusicSerivcesThatRequireAccessibility returns " + answer);
        return answer;
    }


}
