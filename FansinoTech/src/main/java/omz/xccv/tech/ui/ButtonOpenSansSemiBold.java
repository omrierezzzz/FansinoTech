package omz.xccv.tech.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import omz.xccv.tech.FansinoSdk;

public class ButtonOpenSansSemiBold extends Button {

	private static final int THREHOLD_TEXT_LENGTH = 9;
	public ButtonOpenSansSemiBold(Context context) {
		super(context);

		if(!this.isInEditMode())
		{
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/open-sans-semibold.ttf");
		setTypeface(face);
		}
	}
	public ButtonOpenSansSemiBold(Context context, AttributeSet attr) {
		super(context,attr);
		if(!this.isInEditMode())
		{
           try {
               Typeface face = Typeface.createFromAsset(context.getAssets(),
                       "fonts/open-sans-semibold.ttf");
               setTypeface(face);
           }
           catch(Exception e)
           {
               FansinoSdk.logException(e);}
		}

	}
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

	}


//	@Override
//	public void setText(CharSequence text, BufferType type) {
//		// TODO Auto-generated method stub
//		
//		int len=text.length();
//		if (len<THREHOLD_TEXT_LENGTH)
//			this.setWidth(250);
//		super.setText(text, type);
//	}


 /*   @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        //return super.dispatchTouchEvent(event);
        return true;
    }*/
}
