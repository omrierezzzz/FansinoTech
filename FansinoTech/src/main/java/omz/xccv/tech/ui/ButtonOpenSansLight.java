package omz.xccv.tech.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import omz.xccv.tech.FansinoSdk;

public class ButtonOpenSansLight extends Button {

	private static final int THREHOLD_TEXT_LENGTH = 9;
	public ButtonOpenSansLight(Context context) {
		super(context);

		if(!this.isInEditMode())
		{
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/open-sans-light.ttf");
		setTypeface(face);
		}
	}
	public ButtonOpenSansLight(Context context, AttributeSet attr) {
		super(context,attr);
		if(!this.isInEditMode())
		{
           try {
               Typeface face = Typeface.createFromAsset(context.getAssets(),
                       "fonts/open-sans-light.ttf");
               setTypeface(face);
           }
           catch(Exception e)
           {
               FansinoSdk.logException(e);}
		}

	}
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

	}
//	@Override
//	public void setText(CharSequence text, BufferType type) {
//		// TODO Auto-generated method stub
//		
//		int len=text.length();
//		if (len<THREHOLD_TEXT_LENGTH)
//			this.setWidth(250);
//		super.setText(text, type);
//	}

	

}
