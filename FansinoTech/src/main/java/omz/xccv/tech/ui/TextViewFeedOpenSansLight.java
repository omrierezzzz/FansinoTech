package omz.xccv.tech.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import omz.xccv.tech.FansinoSdk;

public class TextViewFeedOpenSansLight extends TextView {

	public TextViewFeedOpenSansLight(Context context) {
		super(context);

		if(!this.isInEditMode())
		{
		Typeface face = Typeface.createFromAsset(context.getAssets(),
				"fonts/open-sans-light.ttf");
		setTypeface(face);
		}
	}
	public TextViewFeedOpenSansLight(Context context, AttributeSet attr) {
		super(context,attr);
		if(!this.isInEditMode())
		{
           try {
               Typeface face = Typeface.createFromAsset(context.getAssets(),
                       "fonts/open-sans-light.ttf");
               setTypeface(face);
           }
           catch(Exception e)
           {
               FansinoSdk.logException(e);}
		}

	}
	protected void onDraw(Canvas canvas) {
        if(!this.isInEditMode()) {
            super.onDraw(canvas);
        }

	}

	

}
