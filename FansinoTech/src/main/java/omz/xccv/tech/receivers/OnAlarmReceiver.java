package omz.xccv.tech.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import omz.xccv.tech.services.FansinoIntentService;

public class OnAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {


        context.startService(new Intent(context, FansinoIntentService.class).setAction(intent.getAction())); //start TaskButlerService
    }
}