package omz.xccv.tech.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import java.util.Set;

import omz.xccv.tech.providers.MyLibTablesDef;
import omz.xccv.tech.services.FansinoIntentService;
import omz.xccv.tech.utils.LogUtils;

public class MyMediaMReceiver extends BroadcastReceiver {


    public static String TAG="FansinoTech.MyMediaReceiver";
    public static String DATA_KEY_ACTION="action";
	public static String DATA_KEY_KEY_SET="extrasKeySet";

	@Override
	
	public void onReceive(Context con, Intent intent) {
        // TODO Auto-generated method stub

        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String action = intent.getAction();
            Set<String> keys = bundle.keySet();
            String[] keysArr = new String[keys.size()];
            keysArr = keys.toArray(keysArr);
            String[] arr = new String[keysArr.length];
            for (int i = 0; i < arr.length; i++) {

                Object obj = bundle.get(keysArr[i]);
                if (obj instanceof String)
                    arr[i] = (String) obj;
                else
                    arr[i] = "" + obj;


            }
            Intent newIntent = new Intent(con, FansinoIntentService.class);
            newIntent.setAction(FansinoIntentService.ACTION_METADATA);
            bundle.putString(DATA_KEY_ACTION, action);
            bundle.putStringArray(DATA_KEY_KEY_SET, keysArr);
            newIntent.putExtras(bundle);
            MyLibTablesDef.init(con);
            con.startService(newIntent);
            LogUtils.logInfo(TAG, "new metadata received:\n" + bundle);
        }
    }
	
	


}