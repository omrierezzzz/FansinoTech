package omz.xccv.tech.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import omz.xccv.tech.utils.LogUtils;
import omz.xccv.tech.utils.Utils;

/**
 * Created by omrierez on 10/29/14.
 */public class PowerConnectionReceiver extends BroadcastReceiver {
    private final String TAG="PowerConnectionReceiver";
    public static String   PREFS_KEY_ALLOW_AUDIO_RECOGITION	 = "allowAudioRecognition";

    @Override
    public void onReceive(Context context, Intent intent) {


        if(intent.getAction() == Intent.ACTION_POWER_CONNECTED) {
            //Handle power connected

            SharedPreferences mPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context.getApplicationContext());
            boolean isListeningIsOn=mPreferences.getBoolean(PREFS_KEY_ALLOW_AUDIO_RECOGITION, false);
            LogUtils.logDebug(TAG,"isListeningIsOn "+isListeningIsOn);

            if (isListeningIsOn)
             Utils.setAlarmRecognizeFromMicWhenPowerPluged(context);

        }


        else if(intent.getAction() == Intent.ACTION_POWER_DISCONNECTED){
            Utils.cancelRecognizeFromMicAlarm(context);
        }

        LogUtils.logDebug(TAG,intent.getAction());


    }
}