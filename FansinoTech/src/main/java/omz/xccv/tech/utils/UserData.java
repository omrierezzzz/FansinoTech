package omz.xccv.tech.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.Serializable;

public class UserData implements Cloneable, Serializable {
	/**
	 * 
	 */
    public static final long serialVersionUID = 6565115447920301462L;
    public static final String PREFS_KEY_USER_PROFILE = "userProfileSeri";
    public static final  String PREFS_KEY_PROFILE_IMAGE = "profileImage";
    public static final String PREFS_KEY_USER_NAME = "userName";
    public static final String PREFS_KEY_PROVIDER = "provider";
    public static final String PREFS_KEY_IS_FIRST_RUN = "isFirstRun";
    public static final String PREFS_KEY_ROLE = "role";
    public static final String ROLE_VAL_ARTIST="artist";
    public static final String ROLE_VAL_FAN="fan";


    public UserData(String displayName, String firstName, String lastName,
			String imageUrl, String gender, String validatedId, String country,
			String city, String birthday, String email, String providerName,boolean isVerified) {
		this.displayName = displayName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.imageUrl = imageUrl;
		this.gender = gender;
		this.validatedId = validatedId;
		this.country = country;
		this.city = city;
		this.birthday = birthday;
		this.email = email;
		this.providerName = providerName;
        this.isVerified=isVerified;

	}

	public static UserData getFromPrefs(Context con) {

		SharedPreferences mPreferences = PreferenceManager
				.getDefaultSharedPreferences(con);
		return (UserData) SerializationObjectsUtil.stringToObject(mPreferences
				.getString(PREFS_KEY_USER_PROFILE, ""));

	}

	public  void updateInPrefs(Context con) {


		SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(con);
		SharedPreferences.Editor editor  = mPreferences.edit();
		String ser = SerializationObjectsUtil.objectToString((UserData)clone());
		editor.putString(PREFS_KEY_USER_PROFILE, ser);
        updateUserProfileImageInPrefs(con);
		editor.commit();
	}
	public  void updateUserProfileImageInPrefs(Context con) {


		SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(con);
		SharedPreferences.Editor editor = mPreferences.edit();
		editor.putString(PREFS_KEY_PROFILE_IMAGE, this.imageUrl);
		editor.commit();
	}

    public  void updateUserNameInPrefs(Context con) {


        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(con);
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PREFS_KEY_USER_NAME, this.displayName);
        editor.commit();
    }
	

	
	
	public UserData() {
	}

	public String displayName;
	public String firstName;
	public String lastName;
	public String imageUrl;
	public String gender;
	public String validatedId;
	public String country;
	public String city;
	public String birthday;
	public String email;
	public String providerName;
    public boolean isVerified;

	@Override
	public Object clone() {
		return new UserData(displayName, firstName, lastName, imageUrl, gender,
				validatedId, country, city, birthday, email, providerName,isVerified);
	}

}