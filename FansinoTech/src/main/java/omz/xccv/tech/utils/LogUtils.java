package omz.xccv.tech.utils;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import omz.xccv.tech.BuildConfig;

/**
 * Created by omrierez on 10/4/14.
 */
public class LogUtils {


    private final static boolean IS_LOGGING_ON= BuildConfig.logging_state;
    //private final static boolean IS_LOGGING_ON= true;

    public static void logInfo(String tag,String msg)
    {
        if (IS_LOGGING_ON)
            Log.i(tag, msg);
    }

    public static void logWarning(String tag,String msg)
    {
        if (IS_LOGGING_ON)
            Log.w(tag, msg);
    }

    public static void logWarning(String tag,String msg,Exception e)
    {
        if (IS_LOGGING_ON) {
            Log.w(tag, msg,e);

        }
    }

    public static void logDebug(String tag,String msg)
    {
        if (IS_LOGGING_ON)
            Log.d(tag, msg);
    }

    public static void logDebug(String tag,String msg,Exception e)
    {
        if (IS_LOGGING_ON) {
            Log.d(tag, msg,e);

        }
    }
    public static void logError(String tag,String msg)
    {
        if (IS_LOGGING_ON)
            Log.e(tag, msg);
        if (BuildConfig.send_crashs_to_crashlytics)
            Crashlytics.log(msg);


    }
    public static void logError(String tag,String msg,Exception e)
    {
        if (IS_LOGGING_ON) {
            Log.e(tag, msg,e);

        }
        if (BuildConfig.send_crashs_to_crashlytics)
            Crashlytics.logException(e);

    }
}
