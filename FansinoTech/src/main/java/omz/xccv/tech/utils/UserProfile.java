package omz.xccv.tech.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.GCMService;
import omz.xccv.tech.interfaces.ServerCallbacks;

public class UserProfile {

	/**
	 * 
	 */
	private static final String TAG = "UserProfile";

	public static final String PROVIDER_FACEBOOK = "facebook";
    public static String KEY_DATA_UNKNOWN = "UNKNOWN";

    private Context mContext;
	public static UserData userData;

	// private EventConsumer myEventConsumer;
	

    public UserProfile(UserData userData,Context con)
    {
        this.userData=userData;
        this.mContext=con.getApplicationContext();

    }

    private String getStringFromJson(JSONObject obj,String key)
    {
        try{
            return obj.getString(key);

        }catch(JSONException e)

        {
          return null;
        }

    }
	public UserProfile(String providerName, JSONObject profile, Context con) {

		// myEventConsumer = event;
		this.mContext = con.getApplicationContext();
		userData = new UserData();
		userData.providerName = providerName;
		try {
			if (providerName.compareTo(PROVIDER_FACEBOOK) == 0) {

				userData.validatedId = getStringFromJson(profile,"id");
				userData.displayName = getStringFromJson(profile,"name");
				userData.firstName = getStringFromJson(profile,"first_name");
				userData.lastName = getStringFromJson(profile,"last_name");
				userData.imageUrl = new JSONObject(new JSONObject(
                        getStringFromJson(profile,"picture")).getString("data"))
						.getString("url");
				userData.gender = getStringFromJson(profile,"gender");
//				String[] location = new JSONObject(
//						profile.getString("location")).getString("name").split(
//						",");
//				if (location.length == 2) {
//					userData.country = location[1].trim();
//					userData.city = location[0].trim();
//				}

				userData.email = getStringFromJson(profile,"email");
				userData.birthday =getStringFromJson(profile,"birthday");
                userData.isVerified=profile.getBoolean("verified");
			}

			validateEmptyFields();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			FansinoSdk.logException(e);
		}

	}

	private void validateEmptyFields() {
		if (userData.displayName == null)
			userData.displayName = KEY_DATA_UNKNOWN;
		if (userData.firstName == null)
			userData.firstName = KEY_DATA_UNKNOWN;
		if (userData.lastName == null)
			userData.lastName = KEY_DATA_UNKNOWN;
		if (userData.imageUrl == null)
			userData.imageUrl = KEY_DATA_UNKNOWN;
		if (userData.gender == null)
			userData.gender = KEY_DATA_UNKNOWN;
		if (userData.country == null)
			userData.country = getCountryISO();
		if (userData.city == null)
			userData.city = KEY_DATA_UNKNOWN;
		if (userData.email == null)
			userData.email = getEmailFormGoogleAccount(mContext);
		if (userData.birthday == null)
			userData.birthday = KEY_DATA_UNKNOWN;
	}

	private void continueRegistrationFlowAfterLogin(String userName,
			String provider, final Context appContext) {


		GCMRegistrar.checkDevice(mContext);
		GCMRegistrar.checkManifest(mContext);
		String regId = GCMRegistrar.getRegistrationId(mContext);
		if (regId.equals("")) {
            try {
                GoogleCloudMessaging.getInstance(mContext).register(GCMService.GCM_SENDER_ID);
            }catch(IOException e)
            {FansinoSdk.logException(e);}
		}
        else
        {
            GCMService.registerUserInServer(mContext, regId, new ServerCallbacks() {
                @Override
                public void userRegistrationFinishedOK(int userID) {
                    // TODO Auto-generated method stub
                    Utils.setIntInPrefs(appContext,Utils.PREFS_KEY_USER_ID, userID);
                    GCMService.sFacebookSemaphore.release();
                }

                @Override
                public void userRegistrationFinishedERROR(String error) {
                    // TODO scheduale user registration to server later
                    LogUtils.logError("GCMService", "userRegistrationFinishedERROR:" + error);
                }
            });

        }
        GCMService.storeRegistrationId(mContext, regId);

		updatePreferences(userName, provider);

	}



	public static String getEmailFormGoogleAccount(Context con) {

		AccountManager manager = (AccountManager) con
				.getSystemService(Context.ACCOUNT_SERVICE);
		Account[] list = manager.getAccounts();
		for (int i = 0; i < list.length; i++) {
			Account account = list[i];
			if (account.type.compareTo("com.google") == 0)
				return account.name;

		}
		return null;

	}

	private String getCountryISO() {
		TelephonyManager telephonyManager = (TelephonyManager) mContext
				.getSystemService(Context.TELEPHONY_SERVICE);
		int simState = telephonyManager.getSimState();

		switch (simState) {

		case (TelephonyManager.SIM_STATE_ABSENT):
			break;
		case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
			break;
		case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
			break;
		case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
			break;
		case (TelephonyManager.SIM_STATE_UNKNOWN):
			break;
		case (TelephonyManager.SIM_STATE_READY): {

			// Get the SIM country ISO code
			String simCountry = telephonyManager.getSimCountryIso();

			// Get the operator code of the active SIM (MCC + MNC)
			String simOperatorCode = telephonyManager.getSimOperator();

			// Get the name of the SIM operator
			String simOperatorName = telephonyManager.getSimOperatorName();

			// Get the SIM�s serial number
			String simSerial = telephonyManager.getSimSerialNumber();
			return simCountry;
		}
		}

		return KEY_DATA_UNKNOWN;

	}

	public void updatePreferences(String displayName, String provider) {
		SharedPreferences mPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		SharedPreferences.Editor editor = mPreferences.edit();
		mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

		editor = mPreferences.edit();

		editor.putString(UserData.PREFS_KEY_USER_NAME, displayName);
		editor.putString(UserData.PREFS_KEY_PROVIDER, provider);
		userData.updateInPrefs(mContext);
		editor.putString(UserData.PREFS_KEY_PROFILE_IMAGE,
				userData.imageUrl);

		editor.commit();
	}

	public void whenLoginComplete(Context appContext) {
		// Variable to receive message status
		LogUtils.logInfo("TAG", "login complete.User profile retrieved and parsed");
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
	    continueRegistrationFlowAfterLogin(userData.displayName,
					userData.providerName,appContext);

	}

}
