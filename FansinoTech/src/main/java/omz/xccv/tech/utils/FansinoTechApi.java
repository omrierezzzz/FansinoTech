package omz.xccv.tech.utils;

import android.content.Context;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import omz.xccv.tech.interfaces.ServerCallbacks;
import omz.xccv.tech.network.NetworkManager;

public class FansinoTechApi implements Callback {

    private static final String TAG = FansinoTechApi.class.getSimpleName();
    public final static String SERVER_ADRRESS = "https://fansinoserver.appspot.com";
    public final static String SERVER_ADRRESS_CONTENT_MANAGER = "http://104.197.12.199:4567";
    public static final MediaType MEDIA_TYPE_MARKDOWN
            = MediaType.parse("text/x-markdown; charset=utf-8");
    public static final MediaType JSON=MediaType.parse("application/json; charset=utf-8");
    public static final MediaType URL_ENCODED_PARAMS=MediaType.parse("application/x-www-form-urlencoded");




    protected static NetworkManager mNetworkManager;
    public final static String REST_PATH_INSERT = "/insert";

    public FansinoTechApi(Context con) {
        if (mNetworkManager==null)
           mNetworkManager=new NetworkManager(con);
    }

    public void registerUser(String role, String fanUserName, String country,
                             String gender, int rank, int playTime,
                             int age, String clientID,
                             String userValidationID, String profileImagePath,
                             String email, String birthday, String city, final ServerCallbacks callBackInterface, final Context appContext) {
        String url = SERVER_ADRRESS + "/" + role + REST_PATH_INSERT;
        RequestBody formBody = prepareRegisterUserParams(fanUserName, country, gender, rank, playTime, age, clientID, userValidationID, profileImagePath, email, birthday, city);
        mNetworkManager.doPostASync(url, formBody, null, new Callback() {
            @Override
            public void onResponse(Response response) throws IOException {
                JSONObject respo;
                try {
                    ResponseBody body=response.body();
                    respo = new JSONObject(body.string());
                    int userId = respo.getInt("id");
                    Utils.setIntInPrefs(appContext, Utils.PREFS_KEY_USER_ID, userId);
                    if (userId < 0)
                        if (callBackInterface != null)
                            callBackInterface
                                    .userRegistrationFinishedERROR(response.body().toString());// /TODO Add error handling
                        else if (callBackInterface != null)
                            callBackInterface
                                    .userRegistrationFinishedOK(userId);
                } catch (JSONException e) {
                    LogUtils.logError(TAG, "Error in registerUser", e);
                }
            }
            @Override
            public void onFailure(Request request, IOException e) {

                e.printStackTrace();
            }
        });

    }

    public void insertUserSync(final String role, final String fanUserName, final String country,
                               final String gender, final int rank, final int playTime,
                               final int age, final String clientID,
                               final String userValidationID, final String profileImagePath,
                               final String email, final String birthday, final String city, final ServerCallbacks callBackInterface, final Context appContext) {

        String url = SERVER_ADRRESS + "/" + role + REST_PATH_INSERT;
        RequestBody formBody = prepareRegisterUserParams(fanUserName, country, gender, rank, playTime, age, clientID, userValidationID, profileImagePath, email, birthday, city);
        String response = mNetworkManager.doPostSync(url, formBody, null);
        JSONObject respo;
        try {
            respo = new JSONObject(response);
            int userId = respo.getInt("id");
            Utils.setIntInPrefs(appContext, Utils.PREFS_KEY_USER_ID, userId);
            if (userId < 0)
                if (callBackInterface != null)
                    callBackInterface
                            .userRegistrationFinishedERROR(response);// /TODO Add error handling
                else if (callBackInterface != null)
                    callBackInterface
                            .userRegistrationFinishedOK(userId);
        } catch (JSONException e) {
            LogUtils.logError(TAG, "Error in registerUser", e);
        }
    }



    private RequestBody prepareRegisterUserParams(String fanUserName, String country, String gender, int rank, int playTime, int age, String clientID, String userValidationID, String profileImagePath, String email, String birthday, String city) {
        return new FormEncodingBuilder()
                .add("userName", fanUserName)
                .add("birthday", birthday)
                .add("city", city)
                .add("country", country)
                .add("gender", gender)
                .add("rank", "" + rank)
                .add("playTime", "" + playTime)
                .add("age", "" + age)
                .add("email", email)
                .add("clientID", clientID)
                .add("profileImagePath", profileImagePath)
                .add("userValidationID", userValidationID)
                .build();
    }

    @Override
    public void onFailure(Request request, IOException e) {

        LogUtils.logError(TAG, "onFailure :" + request.toString(), e);
    }

    public void onFailure(Request request, Exception e) {

        LogUtils.logError(TAG, "onFailure :" + request.toString(), e);
    }

    @Override
    public void onResponse(Response response) throws IOException {
        LogUtils.logDebug(TAG, "onResponse :" + response.toString());

    }
}
