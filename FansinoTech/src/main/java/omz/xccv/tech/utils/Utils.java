package omz.xccv.tech.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import omz.xccv.tech.FansinoSdk;
import omz.xccv.tech.R;
import omz.xccv.tech.constants.TimeConstants;
import omz.xccv.tech.receivers.OnAlarmReceiver;
import omz.xccv.tech.services.FansinoIntentService;

/**
 * Created by omrierez on 8/1/14.
 */


public class Utils {
    public static String PREFS_KEY_USER_ID = "userID";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String TAG="FansinoTech.Utils";
    private static  Context con;
    private static  PendingIntent recognizeFromMicIntent;
    private static final long TIME_INTERVAL_FOR_RECOGNIZE_FROM_MIC_ACTION= TimeConstants.MINUTE*3;



    public static  PendingIntent setAlarm(Context context,String action,long timeInterval){
        AlarmManager alarmManager=(AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context,OnAlarmReceiver.class).setAction(action);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), timeInterval, pendingIntent);
        return pendingIntent;
    }
    public static  void cancelAlarm(Context context,PendingIntent intent){
        if( intent!=null) {
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
           alarmManager.cancel(intent);

        }


    }


    public static void setAlarmRecognizeFromMicWhenPowerPluged(Context con)
    {

        recognizeFromMicIntent=Utils.setAlarm(con, FansinoIntentService.ACTION_TRY_TO_RECOGNIZE_FROM_MIC, TIME_INTERVAL_FOR_RECOGNIZE_FROM_MIC_ACTION);

    }

    public static void cancelRecognizeFromMicAlarm(Context con)
    {
        cancelAlarm(con,recognizeFromMicIntent);
        recognizeFromMicIntent=null;
    }
    public static Map<String, Integer> sortMapByComparator(Map<String, Integer> unsortMap,final boolean isDesc) {

        // Convert Map to List
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2) {
                if (isDesc)
                 return (o2.getValue()).compareTo(o1.getValue());
                else
                 return (o1.getValue()).compareTo(o2.getValue());

            }
        });

        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }


    public static void setContext(Context context)
    {

        con=context;
    }

    public static void setIntInPrefs(Context con,String key, int val) {

        SharedPreferences mPreferences = PreferenceManager
                .getDefaultSharedPreferences(con);
        mPreferences.edit().putInt(key, val)
                .commit();

    }
    public static void setStringInPrefs(Context con,String key, String val) {

        SharedPreferences mPreferences = PreferenceManager
                .getDefaultSharedPreferences(con);
        mPreferences.edit().putString(key, val)
                .commit();

    }

    public static String getStringFromPrefs(Context con,String key) {

        SharedPreferences mPreferences = PreferenceManager
                .getDefaultSharedPreferences(con);
        return mPreferences.getString(key,"");


    }


    public static int getIntFromPrefs(String key) {

        SharedPreferences mPreferences = PreferenceManager
                .getDefaultSharedPreferences(con);
        return mPreferences.getInt(key,0);


    }

    public static boolean checkPlayServices(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                LogUtils.logInfo(TAG, "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    public static  SharedPreferences getGCMPreferences(Activity activity) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return activity.getSharedPreferences(activity.getClass().getSimpleName(),
                Context.MODE_PRIVATE);
    }


    public static  SharedPreferences getGCMPreferences(Service service) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return service.getSharedPreferences(service.getClass().getSimpleName(),

                Context.MODE_PRIVATE);
    }

    public static  SharedPreferences getGCMPreferences(Context con) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        if(con instanceof Service) {
            Service service = (Service) con;
            return service.getSharedPreferences(service.getClass().getSimpleName(),Context.MODE_PRIVATE);
        }
        else if( con instanceof Activity)
        {
            Activity activity = (Activity) con;
            return activity.getSharedPreferences(activity.getClass().getSimpleName(),Context.MODE_PRIVATE);

        }
        else if( con instanceof Application)
        {
            Application app = (Application) con;
            return app.getSharedPreferences(app.getClass().getSimpleName(),Context.MODE_PRIVATE);

        }
        else
           return null;


    }

    public static  String getRegistrationId(Context con) {
        final SharedPreferences prefs = getGCMPreferences(con);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            LogUtils.logInfo(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(con);
        return registrationId;
        //if (registeredVersion != currentVersion) {
        //    LogUtils.logInfo(TAG, "App version changed.");
       //     return "";
       // }
      //  return registrationId;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    public static String inputStreamToString(InputStream is) {

        try {

            BufferedReader r = new BufferedReader(new InputStreamReader(is));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);

            }
            return total.toString();

        } catch (IOException e) {
            FansinoSdk.logException(e);
        }


        return null;
    }

    public static String readContentFromAssetFile(AssetManager assetManager,String fileName) {
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(fileName);
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            String content = stringBuilder.toString();
            return content;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            FansinoSdk.logException(e);
        }


        return null;
    }

    public static String simpleHttpGet(String url)
    {

        HttpClient httpclient = new DefaultHttpClient();

        // Prepare a request object
        HttpGet httpget = new HttpGet(url);

        // Execute the request
        HttpResponse response;
        try {
            response = httpclient.execute(httpget);
            // Examine the response status
            LogUtils.logInfo("Praeda",response.getStatusLine().toString());

            // Get hold of the response entity
            HttpEntity entity = response.getEntity();
            // If the response does not enclose an entity, there is no need
            // to worry about connection release

            if (entity != null) {

                // A Simple JSON Response Read
                InputStream instream = entity.getContent();
                String result= convertStreamToString(instream);
                // now you have the string representation of the HTML request
                instream.close();
                return result;

            }


        } catch (Exception e) {
            FansinoSdk.logException(e);

        }
        return null;
    }

    private static String convertStreamToString(InputStream is) {
    /*
     * To convert the InputStream to String we use the BufferedReader.readLine()
     * method. We iterate until the BufferedReader return null which means
     * there's no more data to read. Each line will appended to a StringBuilder
     * and returned as String.
     */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            FansinoSdk.logException(e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                FansinoSdk.logException(e);
            }
        }
        return sb.toString();
    }

    public static boolean hasJellyBeansSdk18OrHigher() {
        int sdkVersion=Build.VERSION.SDK_INT;
        return sdkVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    public static boolean hasSdk20OrHigher() {
        int sdkVersion=Build.VERSION.SDK_INT;
        return sdkVersion >= Build.VERSION_CODES.KITKAT_WATCH;
    }

    public static  void showAlertDialog(Context con,String title,String text,int iconResId,boolean addOkButton) {
        AlertDialog alertDialog1 = new AlertDialog.Builder(
                con).create();
        alertDialog1.setTitle(title);
        alertDialog1.setMessage(text);
        alertDialog1.setIcon(iconResId);
        if (addOkButton)
          alertDialog1.setButton(AlertDialog.BUTTON_POSITIVE,con.getString(R.string.ok),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
           });
        alertDialog1.show();
    }

    public static  AlertDialog initAlertDialog(Context con,String title,String text,int iconResId) {
        AlertDialog alertDialog1 = new AlertDialog.Builder(
                con).create();
        alertDialog1.setTitle(title);
        alertDialog1.setMessage(text);
        alertDialog1.setIcon(iconResId);

        return alertDialog1;
    }

    public static String getAppHashKey(Context con) {

        try {

            PackageInfo info = con.getPackageManager().getPackageInfo(con.getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toString().getBytes());
                String hashCode  = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("KeyHash:", hashCode);
                return hashCode;
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        return null;
    }

    public void devGenerateException() {
        String bla=null;
        bla.isEmpty();
    }
}
