/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omz.xccvlab.fansino.utils;

public class Config {
    // General configuration
    public static final boolean IS_PRODUCTION = true;
    public static final boolean LOAD_FAN_FEED = true;
    public static final boolean SEND_EXCEPTIONS_TO_CRASHLYTICS = true;
    public static final boolean SEND_ANALYTICALS_EVENTS = true;


}
